import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    EMAIL_CHANGED,
    PHONE_CHANGED,
    OTP_CHANGED
} from '../actions/types'

let appSate = {
    showLoading: false,
    error: false,
    id: '',
    otp: '123',
    data: {}
}

const loginReducer = (state = appSate, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                showLoading: true,
                error: false,
            }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                showLoading: false,
                error: false,
                data: action.payload

            }
        case LOGIN_USER_FAIL:
            return {
                ...state,
                showLoading: false,
                error: true,
            }
        case OTP_CHANGED:
            return {
                ...state,
                showLoading: false,
                error: false,
                otp: action.payload,
            }
        default:
            return state;
    }
}

export default loginReducer;