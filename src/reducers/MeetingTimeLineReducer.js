import {
    MEETINGTIMELINE_FETCHING,
    MEETINGTIMELINE_FETCHING_SUCCESS,
    MEETINGTIMELINE_FETCHING_FAIL,
    MEETINGTIMELINE_CHECK_CALENDAR,
    MEETINGTIMELINE_UNCHECK_CALENDAR,
    MEETINGTIMELINE_STATUS,
    MEETINGTIMELINE_NOTE_SUCCESS,
    MEETINGTIMELINE_NOTE_FAIL
} from '../actions/types'

let appSate = {
    isLoading: false,
    error: false,
    data: {},
    timelines: [],
    status: []
}

export default (state = appSate, action) => {
    let newData = state.timelines;
    switch (action.type) {
        case MEETINGTIMELINE_FETCHING:
            return {
                ...state,
                isLoading: true
            }
        case MEETINGTIMELINE_FETCHING_SUCCESS:
            let data = action.payload;
            let stus = [];
            for (let i = 0; i < data.length; i++) {
                if (i == 0) stus.push(true);
                stus.push(false);
            }
            return {
                ...state,
                isLoading: false,
                data: action.payload,
                timelines: action.timelines,
                status: stus
            }
        case MEETINGTIMELINE_FETCHING_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        case MEETINGTIMELINE_CHECK_CALENDAR:
            newData[action.atIndex].is_calendar_checked = newData[action.atIndex].is_calendar_checked == 0 ? 1 : 0;
            newData[action.atIndex].calendar_id = action.calendar_id
            return {
                ...state,
                timelines: newData
            }
        case MEETINGTIMELINE_UNCHECK_CALENDAR:
            newData[action.atIndex].is_calendar_checked = 0
            return {
                ...state,
                timelines: newData
            }
        case MEETINGTIMELINE_STATUS:
            let dataSet = state.data;
            let status = state.status;
            for (let i = 0; i < dataSet.length; i++) {
                status[i] = false
            }
            if (dataSet.length > 0) {
                status[action.atIndex] = true
            }
            return {
                ...state,
                status: status,
            }
        case MEETINGTIMELINE_NOTE_SUCCESS:
       
            newData[action.atIndex].note = action.note;
            return {
                ...state,
                timelines: newData
            }
        case MEETINGTIMELINE_NOTE_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        default:
            return state;
    }
    return state;
}
