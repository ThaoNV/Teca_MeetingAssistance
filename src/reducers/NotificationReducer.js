import {
    NOTIFICATION_FETCHING,
    NOTIFICATION_FETCHING_SUCCESS,
    NOTIFICATION_FETCHING_FAIL,
    NOTIFICATION_FETCHING_ITEM,
    NOTIFICATION_FETCHING_ITEM_SUCCESS,
    NOTIFICATION_FETCHING_ITEM_FAIL,
    NOTIFICATION_ITEM_CLOSE
} from '../actions/types'

let appSate = {
    isLoading: false,
    error: false,
    data: [],
    showitem: false,
    dataitem: []
}

export default (state = appSate, action) => {
    let newData = state.data;
    switch (action.type) {
        case NOTIFICATION_FETCHING:
            return {
                ...state,
                isLoading: true
            }
        case NOTIFICATION_FETCHING_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.payload
            }
        case NOTIFICATION_FETCHING_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        case NOTIFICATION_FETCHING_ITEM:
            return {
                ...state,
                isLoading: true
            }
        case NOTIFICATION_FETCHING_ITEM_SUCCESS:
            return {
                ...state,
                isLoading: false,
                showitem: true,
                dataitem: action.payload
            }
        case NOTIFICATION_FETCHING_ITEM_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        case NOTIFICATION_ITEM_CLOSE:
            return {
                ...state,
                showitem: false,
            }
        default:
            return state;
    }
    return state;
}