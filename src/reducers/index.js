import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer';
import RegisterReducer from './RegisterReducer';
import CategoryMeetingReducer from './CategoryMeetingReducer';
import MeetingReducer from './MeetingReducer';
import MeetingTimeLineReducer from './MeetingTimeLineReducer';
import NotificationReducer from './NotificationReducer';
import NotificationDetailReducer from './NotificationDetailReducer';
import AboutUsReducer from './AboutUsReducer';
import SupportReducer from './SupportReducer';

export default combineReducers({
    loginReducer: LoginReducer,
    registerReducer: RegisterReducer,
    categoryMeetingReducer: CategoryMeetingReducer,
    meetingReducer: MeetingReducer,
    meetingTimeLineReducer: MeetingTimeLineReducer,
    notificationReducer: NotificationReducer,
    notificationDetailReducer: NotificationDetailReducer,
    aboutUsReducer: AboutUsReducer,
    supportReducer: SupportReducer
});