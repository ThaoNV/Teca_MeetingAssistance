import {
    NOTIFICATION_DETAIL_FETCHING,
    NOTIFICATION_DETAIL_FETCHING_SUCCESS,
    NOTIFICATION_DETAIL_FETCHING_FAIL,
    NOTIFICATION_DETAIL_STATUS
} from '../actions/types'

let appSate = {
    isLoading: false,
    error: false,
    data: [],
    status: []
}

export default (state = appSate, action) => {
    switch (action.type) {
        case NOTIFICATION_DETAIL_FETCHING:
            return {
                ...state,
                isLoading: true
            }
        case NOTIFICATION_DETAIL_FETCHING_SUCCESS:
            let data = action.payload;
            let stus = [];
            for (let i = 0; i < data.length; i++) {
                if (i == 0) stus.push(true);
                stus.push(false);
            }
            return {
                ...state,
                isLoading: false,
                data: action.payload,
                status: stus
            }
        case NOTIFICATION_DETAIL_FETCHING_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        case NOTIFICATION_DETAIL_STATUS:
            let dataSet = state.data;
            let status = state.status;
            for (let i = 0; i < dataSet.length; i++) {
                status[i] = false
            }
            if (dataSet.length > 0) {
                status[action.atIndex] = true
            }
            return {
                ...state,
                status: status,

            }
        default:
            return state;
    }
    return state;
}
