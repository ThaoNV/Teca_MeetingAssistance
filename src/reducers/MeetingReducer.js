import {
    MEETING_FETCHING,
    MEETING_FETCHING_SUCCESS,
    MEETING_FETCHING_FAIL,
    MEETING_CHECK_CALENDAR,
    MEETING_UNCHECK_CALENDAR
} from '../actions/types'

let appSate = {
    isLoading: false,
    error: false,
    data: []
}

export default (state = appSate, action) => {
    let newData = state.data;
    switch (action.type) {
        case MEETING_FETCHING:
            return {
                ...state,
                isLoading: true
            }
        case MEETING_FETCHING_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.payload
            }
        case MEETING_FETCHING_FAIL:
            return {
                ...state,
                isLoading: false,
                error: true
            }
        case MEETING_CHECK_CALENDAR:
            newData[action.atIndex].is_calendar_checked = newData[action.atIndex].is_calendar_checked == 0 ? 1 : 0;
            newData[action.atIndex].calendar_id = action.calendar_id
            return {
                ...state,
                data: newData
            }
        case MEETING_UNCHECK_CALENDAR:
            newData[action.atIndex].is_calendar_checked = 0
            return {
                ...state,
                data: newData
            }
        default:
            return state;
    }
    return state;
}
