import {
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    EMAIL_CHANGED,
    PHONE_CHANGED,
    EMAIL_FAIL,
    PHONE_FAIL
} from '../actions/types'

let appSate = {
    isLoading: false,
    press: false,
    email: 'test@demo.com',
    phone: '19001234',
    status: 0,
    loginError: '',
    msg: '',
}

export default (state = appSate, action) => {
    switch (action.type) {
        case EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload,
            }
        case EMAIL_FAIL:
        
            return {
                ...state,
                isLoading: false,
                loginError: 'Định dạng email không chính xác!'
            }
        case PHONE_CHANGED:
            return {
                ...state,
                phone: action.payload,
            }
        case PHONE_FAIL:
            return {
                ...state,
                isLoading: false,
                loginError: action.payload,
            }
        case REGISTER_USER:
    
            return {
                ...state,
                isLoading: true,             
            }
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                isLoading: false
            }
        case REGISTER_USER_FAIL:
            return {
                ...state,
                isLoading: false,
                msg: 'Register Failed!'
            }
        default:
            return state;
    }
    return state;
}
