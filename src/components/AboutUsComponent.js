
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    Text,
    View
} from 'react-native';

import { fetchAboutUs, AboutUsStatus } from '../actions/AboutUsAction';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';

export class AboutUsComponent extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.fetchAboutUs();
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.content}>
                        <HTML style={styles.categoryChildText} html={this.props.listData.data.about} {...this.props} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    content: {
        padding: 10
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    categoryChildText: {
        fontSize: 12,
    },
};

export default connect(
    state => {
        return {
            listData: state.aboutUsReducer,
            isLoading: state.aboutUsReducer.isLoading,
            error: state.aboutUsReducer.error,
            status: state.aboutUsReducer.status
        }
    },
    dispatch => {
        return {
            fetchAboutUs: () => dispatch(fetchAboutUs()),
            AboutUsStatus: (index) => dispatch(AboutUsStatus(index))
        }
    }
)(AboutUsComponent);
