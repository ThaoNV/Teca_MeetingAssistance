
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    ActivityIndicator,
    FlatList,
    TouchableOpacity,
    View
} from 'react-native';
import CheckBox from 'react-native-check-box'
import { fetchMeeting, meetingCalendarMeeting, meetingCheckCalendar, meetingUnCheckCalendar } from '../actions/MeetingAction';
import { connect } from 'react-redux';
import Card from '../containers/Card';
import CardSection from '../containers/CardSection';
import CardItem from '../containers/CardItem';
import RNCalendarEvents from 'react-native-calendar-events';


export class MeetingComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            bgColor: [
                '#e84e40',
                '#ec407a',
                '#ab47bc',
                '#7e57c2',
                '#5c6bc0',
                '#738ffe',
                '#29b6f6',
                '#26c6da',
                '#26a69a',
                '#8d6e63',
                '#78909c'
            ],
            selectedColor: ''
        }
    }
    componentDidMount() {
       
        this.props.fetchMeeting(
            this.props.navigation.state.params.item.id, 
            this.props.navigation.state.params.userid,
            this.props.navigation.state.params.uuid        
        );
    }


    renderHeader = () => {
        if (!this.props.isLoading) return null;
        return (
            <View
                style={{
                    //  position: 'absolute',
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    zIndex: 1000,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    _onRefresh(){
        this.props.fetchMeeting(this.props.navigation.state.params.item.id, this.props.navigation.state.params.userid);
    }

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "86%",
              backgroundColor: "#CED0CE",
              marginLeft: "14%"
            }}
          />
        );
      };

    renderItem = ({ item, index }) => {
        const bgItem = this.state.bgColor[Math.floor(Math.random()*this.state.bgColor.length)];
        console.log(bgItem);
        // this.setState({
        //     selectedColor: bgItem,
        //   });
        const { meetingCheckCalendar, meetingUnCheckCalendar, meetingCalendarMeeting } = this.props;
        const time = item.start_time + ' - ' + item.end_time;
        this._checkBoxCalendar = (item, index) => {
            let startDate = new Date(item.start_time_ts);
            let endDate = new Date(item.end_time_ts);
            RNCalendarEvents.authorizeEventStore();
            RNCalendarEvents.saveEvent(item.name, {
                startDate: startDate.toISOString(),
                endDate: endDate.toISOString(),
                location: item.location
            }).then(event => {
                meetingCalendarMeeting(
                    `${event}`, 
                    "check", 
                    this.props.navigation.state.params.userid, 
                    item.id,
                    this.props.navigation.state.params.uuid  
                );
                meetingCheckCalendar(index, event);
            });

        }
        this._unCheckBoxCalendar = (item, index) => {
            meetingUnCheckCalendar(index);
            meetingCalendarMeeting(
                item.calendar_id, 
                "uncheck", 
                this.props.navigation.state.params.userid, 
                item.id,
                this.props.navigation.state.params.uuid
            );
            RNCalendarEvents.authorizeEventStore();
            RNCalendarEvents.removeEvent(`${item.calendar_id}`).then(event => {
                console.log(event);
            })
                .catch(error => {
                    console.log(error);
                });
        }
        this._onPressItem = (item) => {
            const { navigate } = this.props.navigation;
            return navigate('MeetingTimeLayout', { 
                item: item, 
                user_id: this.props.navigation.state.params.userid,
                uuid : this.props.navigation.state.params.uuid
            });
        }
        return (
            <View style={styles.servicesStyle}>
                <Card>
                    <CardItem onPress={() => this._onPressItem(item)}>
                        <View style={styles.card}>
                            <View style={styles.thumbnaiContainerlStyle}>
                                {/* {
                                    item.is_calendar_checked == 0 ?
                                        (
                                            <CheckBox isChecked={false} onClick={() => this._checkBoxCalendar(item, index)} />
                                        )
                                        :
                                        (
                                            <CheckBox isChecked={true} onClick={() => this._unCheckBoxCalendar(item, index)} />
                                        )
                                } */}

                            </View>
                            <View style={styles.thumbnaiContainerlStyle}>
                                {
                                    item.icon_path != null ? (
                                        <Image
                                            style={styles.thumbnailStyle}
                                            source={{uri: `${this.icon_path}`}} />
                                    ) : (
                                            <View style={[styles.circleImage, {backgroundColor: bgItem}]}>
                                                {/* <Text style={styles.circleText}>{item.name.slice(0,1).toUpperCase()}</Text> */}
                                                <Text style={styles.circleText}>VPQH</Text>
                                            </View>
                                        )
                                }


                            </View>
                            <View style={styles.textContentStyle}>
                                <Text style={styles.textStyle}>
                                    {item.name.length < 30 ? item.name : item.name.slice(0, 30) + '…'}
                                </Text>
                                <Text style={styles.textStyleChild}>Thời gian: {time.length < 25 ? time : time.slice(0, 25) + '…'}</Text>
                                <View style={styles.starCalendar}>
                                    <Text style={styles.textStyleChild}>Địa chỉ: {item.location.length < 22 ? item.location : item.location.slice(0, 22) + '…'}</Text>
                                    <View style={styles.starLeft}>
                                        {
                                            item.is_calendar_checked == 0 ?
                                                (
                                                    <TouchableOpacity
                                                        onPress={() => this._checkBoxCalendar(item, index)}
                                                    >
                                                        {/* <Image
                                                            source={require('../images/sao1.png')} /> */}
                                                        <Image style={{ width: 25, height: 25 }} source={require('../images/lichcanhan.png')} />
                                                    </TouchableOpacity>
                                                )
                                                :
                                                (
                                                    <TouchableOpacity
                                                        onPress={() => this._unCheckBoxCalendar(item, index)}
                                                    >
                                                        {/* <Image
                                                            source={require('../images/sao2.png')} /> */}
                                                        <Image style={{ width: 25, height: 25 }} source={require('../images/lich_red.png')} />
                                                    </TouchableOpacity>
                                                )
                                        }
                                    </View>
                                </View>
                            </View>
                            {/* <View style={styles.thumbnaiContainerlStyle}>
                                <Image
                                    source={require('../images/if_arrowright_1167967.png')} />
                            </View> */}

                        </View>
                    </CardItem>
                </Card>
            </View>
        )
    }

    render() {
        const { navigate } = this.props.navigation;
        const dataList = this.props.listData.data;
        return (
            <FlatList
                onEndReachedThreshold = {0.7}
                data={dataList}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.props}
                renderItem={this.renderItem}
                ItemSeparatorComponent={this.renderSeparator}
                refreshing={this.props.isLoading}
                ListHeaderComponent={this.renderHeader}
                onRefresh = {this._onRefresh.bind(this)}
            />
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#e9ebee',
    },
    contentStyle: {
        flex: 1,
        flexDirection: 'column',

    },
    profileStyle: {
        flexDirection: 'row',
        backgroundColor: '#F8F8F8',
        padding: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 20 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    profileImageStyle: {
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        // width: '100%',
        // height: '100%',
    },
    textContentStyle: {
        flex: 1,
        marginLeft: 5

    },
    textStyle: {
        fontWeight: 'bold',
        textAlign: 'left',
        alignItems: 'center',
        color: '#202124',
        width: '100%',
        fontSize: 17
    },
    textStyleChild: {
        fontSize: 15
    },
    profileContentStyle: {
        flex: 1,
        margin: 5,
        width: '100%'
    },
    servicesStyle: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
    },
    scroollStyle: {
        //  flex:1
        // paddingVertical: 10
    },
    thumbnaiContainerlStyle: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    thumbnailStyle: {
        height: 50,
        width: 50,
    },
    imageForHomeHeader: {
        width: 45,
        height: 45
    },
    thumbnailBottomStyle: {
        height: 50,
        width: 50,
    },
    card: {
        flexDirection: 'row',
    },
    starCalendar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    starLeft: {
        justifyContent: 'flex-end',
    },
    live: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#333'
    },
    circleImage: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
     //   backgroundColor: '#4267b2',
        width: 45,
        height: 45,
        // padding: 15
    },
    circleText: {
       // fontSize: 25,
         fontWeight: 'bold',
        color: '#fff'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4267b2',
        height: 40,
        borderRadius: 20,
        zIndex: 100,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
    },
    button_expired: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e12d2d',
        height: 40,
        borderRadius: 20,
        zIndex: 100,
        shadowColor: '#333',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
    },
    text: {
        paddingLeft: 10,
        paddingRight: 10,
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold',
        backgroundColor: 'transparent',
    },
};

// const mapStateToProps = ({meetingReducer}) => {
//     const { isLoading, error, data } = meetingReducer;
//     return { isLoading, error, data };
// };
// export default connect(mapStateToProps, { fetchMeeting, meetingCalendarMeeting, meetingCheckCalendar, meetingUnCheckCalendar })(MeetingComponent);


export default connect(
    state => {
        return {
            listData: state.meetingReducer,
            isLoading: state.meetingReducer.isLoading,
            error: state.meetingReducer.error
        }
    },
    dispatch => {
        return {
            fetchMeeting: (type, userid, uuid) => dispatch(fetchMeeting(type, userid, uuid)),
            meetingCalendarMeeting: (calendar_id, status, user_id, meeting_id, uuid) => dispatch(meetingCalendarMeeting(calendar_id, status, user_id, meeting_id, uuid)),
            meetingCheckCalendar: (index, calendar_id) => dispatch(meetingCheckCalendar(index, calendar_id)),
            meetingUnCheckCalendar: (index) => dispatch(meetingUnCheckCalendar(index))
        }
    }
)(MeetingComponent);