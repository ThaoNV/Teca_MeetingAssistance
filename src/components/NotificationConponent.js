import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    FlatList,
    View,
    AsyncStorage,
    Dimensions,
    Modal,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    WebView,
    Platform
} from 'react-native';
import { fetchNotification, fetchNotificationItem, closeNotificationItem } from '../actions/NotificationAction';
import { connect } from 'react-redux';
import Card from '../containers/Card';
import CardSection from '../containers/CardSection';
import CardItem from '../containers/CardItem';

export class NotificationConponent extends Component {

    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            userid: '',
            bgColor: [
                '#e84e40',
                '#ec407a',
                '#ab47bc',
                '#7e57c2',
                '#5c6bc0',
                '#738ffe',
                '#29b6f6',
                '#26c6da',
                '#26a69a',
                '#8d6e63',
                '#78909c'
            ],
        };
    }

    componentDidMount() {
        console.log("NotificationConponent");
        if (this.props.navigation.state.params != undefined) {
            const userid = this.props.navigation.state.params.user_id;
            this.setState({ userid: userid });
            this.props.fetchNotification(userid);
        } else {
            this.initStart();
        }
    }


    initStart = async () => {
        try {
            const userid = await AsyncStorage.getItem('userid');
            this.setState({ userid: userid });
            this.props.fetchNotification(userid);
        } catch (error) {
            console.log(error);
        }
    }

    renderItem = ({ item }) => {
        const bgItem = this.state.bgColor[Math.floor(Math.random() * this.state.bgColor.length)];
        this._onPressItem = (item) => {
          //  this.props.fetchNotificationItem(this.state.userid, item.id);
            const { navigate } = this.props.navigation;
            return navigate('NotificationDetailLayout', { user_id: this.state.userid, noti_id: item.id, name: item.title });
        }
        return (
            <View style={styles.servicesStyle}>
                <Card>
                    <CardItem onPress={() => this._onPressItem(item)}>
                        {/* <View>
                            <Text style={styles.noti_title}>{item.title}</Text>
                            <Text style={styles.noti_short_des}>{item.description}</Text>
                        </View> */}
                        <View style={styles.card}>
                            <View style={styles.thumbnaiContainerlStyle}>
                                <View style={[styles.circleImage, { backgroundColor: bgItem }]}>
                                    <Text style={styles.circleText}>VPQH</Text>
                                </View>
                            </View>
                            <View style={styles.textContentStyle}>
                                <Text style={styles.textStyle}>
                                    {item.title.length < 35 ? item.title : (item.title.slice(0, 35) + '…')}
                                </Text>
                                <Text style={styles.textStyleChild}>{item.description < 50 ? item.description : (item.description.slice(0, 50) + '…')}</Text>

                            </View>
                        </View>
                    </CardItem>
                </Card>
            </View>
        )
    }
    _onCloseItem = () => {
        this.props.closeNotificationItem();
    }

    renderHeader = () => {
        if (!this.props.isLoading) return null;
        return (
            <View
                style={{
                    //  position: 'absolute',
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    zIndex: 1000,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    _onRefresh() {
        this.props.fetchNotification(this.state.userid);
    }

    render() {
        const { navigate } = this.props.navigation;
        const dataList = this.props.listData.data;
        const dataItem = this.props.dataItem;
        console.log('ABCSSD');
        console.log(this.props.dataItem);
        return (
            <FlatList
                data={dataList}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.props}
                renderItem={this.renderItem}
                refreshing={this.props.isLoading}
                ListHeaderComponent={this.renderHeader}
                onRefresh={this._onRefresh.bind(this)}
            />
        );
    }
}
const win = Dimensions.get('window');
const styles = {
    servicesStyle: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
    },
    item_container: {
        padding: 10,
        justifyContent: 'flex-start',
        borderColor: '#aaaaaa',
        borderBottomWidth: 1,
        width: win.width
    },
    noti_title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#202124',
        alignItems: 'center',
        textAlign: 'left',
        fontSize: 17
    },
    noti_short_des: {
        color: "#777777",
        textAlign: 'left'
    },
    noti_item_title: {
        position: 'absolute',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000000',
        width: win.width,
        textAlign: 'center',
        top: 30,
        padding: 20
    },
    noti_content: {
        marginTop: 100
    },
    container: {
        flex: 1,
        backgroundColor: '#e9ebee',
    },
    vModal: {
        ...Platform.select({
            ios: {
                paddingTop: 80
            },
            android: {
                marginTop: 0
            }
        })
    },
    circleImage: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        width: 45,
        height: 45,
    },
    circleText: {
        fontWeight: 'bold',
        color: '#fff'
    },
    card: {
        flexDirection: 'row',
    },
    thumbnaiContainerlStyle: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textContentStyle: {
        flex: 1,
        marginLeft: 5
    },
    textStyle: {
        fontWeight: 'bold',
        textAlign: 'left',
        alignItems: 'center',
        color: '#202124',
        width: '100%',
        fontSize: 17
    },
    textStyleChild: {
        fontSize: 15
    },
};

export default connect(
    state => {
        return {
            listData: state.notificationReducer,
            isLoading: state.notificationReducer.isLoading,
            error: state.notificationReducer.error,
            dataItem: state.notificationReducer.dataitem,
            showItem: state.notificationReducer.showitem
        }
    },
    dispatch => {
        return {
            fetchNotification: (userid) => dispatch(fetchNotification(userid)),
            fetchNotificationItem: (userid, notificationid) => dispatch(fetchNotificationItem(userid, notificationid)),
            closeNotificationItem: () => dispatch(closeNotificationItem())
        }
    }
)(NotificationConponent);