
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    Text,
    View
} from 'react-native';

import { fetchAboutUs, AboutUsStatus } from '../actions/AboutUsAction';
import { connect } from 'react-redux';

export class EvaluateComponent extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.fetchAboutUs();
    }
    render() {
        return (
            <Text style={[styles.ftext]}>{this.props.listData.data.feedback}</Text>
        );
    }
}

const styles = {
    ftext: {
        paddingLeft: 10,
        fontSize: 14
    }
};

export default connect(
    state => {
        return {
            listData: state.aboutUsReducer,
            isLoading: state.aboutUsReducer.isLoading,
            error: state.aboutUsReducer.error,
            status: state.aboutUsReducer.status
        }
    },
    dispatch => {
        return {
            fetchAboutUs: () => dispatch(fetchAboutUs()),
            AboutUsStatus: (index) => dispatch(AboutUsStatus(index))
        }
    }
)(EvaluateComponent);
