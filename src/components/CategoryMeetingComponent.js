
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    Animated,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    View
} from 'react-native';

import { fetchCategoryMeeting, categoryMeetingStatus } from '../actions/CategoryMeetingAction';
import { connect } from 'react-redux';
import CategoryMeetingList from '../containers/CategoryMeetingList';
import thunk from 'redux-thunk';


const IMAGEBG = [
    {
        slug: 'ky-hop-quoc-hoi',
        source: require('../images/bg-01.png')
    },
    {
        slug: 'phien-hop-ubtv-qh',
        source: require('../images/bg-02.png')
    },
    {
        slug: 'ky-hop-dbqh-ct',
        source: require('../images/bg-03.png')
    },
    {
        slug: 'ky-hop-dbub',
        source: require('../images/bg-04.png')
    }
]
export class CategoryMeetingComponent extends Component {

    constructor(props) {
        super(props);
        
        this.state = { user: null, user_id: null }
    }


    componentDidMount() {
      
        if (this.props.navigation.state.params != undefined) {
            const dt = this.props.navigation.state.params.data;
            this.props.fetchCategoryMeeting(dt.id);
            this.setState({ user: dt, user_id: dt.id });
        } else {
            this.initStart();
        }
    }

    initStart = async () => {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value != null) {
                const data = JSON.parse(value);
                this.props.fetchCategoryMeeting(data.id);
                this.setState({ user: data, user_id: data.id });
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    componentWillMount() {
        console.log(this.props);
    }

    _onMove = (index) => {
        this.props.categoryMeetingStatus(index);
    };

    _getSource = (item) => {
        let image = IMAGEBG.filter(function (value) { return value.slug === item.slug });
        if (image.length > 0) {
            return image[0].source
        }
        return null;
    }

    renderHeader = () => {
        if (!this.props.isLoading) return null;
        return (
            <View
                style={{
                    //  position: 'absolute',
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    zIndex: 1000,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    _onRefresh() {
        this.props.fetchCategoryMeeting(this.state.user_id);
    }

    renderItem = ({ item, index }) => {

        return (
            <CategoryMeetingList
                {...item}
                index={index}
                status={status}
                navigate={this.props.navigation}
                onMove={() => this._onMove(index)}
            />
        )
    }

    render() {
        const { navigate } = this.props.navigation;
        const dataList = this.props.listData.data;
        const status = this.props.status;
        
        return (
            <View style={styles.containers}>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={1}
                    data={dataList}
                    extraData={this.props}
                    renderItem={({ item, index }) => (
                        <CategoryMeetingList
                            {...item}
                            index={index}
                            status={status}
                            length={dataList.length}
                            navigate={this.props.navigation}
                            onMove={() => this._onMove(index)}
                            source={this._getSource(item)}
                            {...this.props}
                        />
                    )}
                    refreshing={this.props.isLoading}
                    ListHeaderComponent={this.renderHeader}
                    onRefresh={this._onRefresh.bind(this)}
                />
            </View>
        );
    }
}
const styles = {
    containers: {
        flex: 1
    }
}
// const mapStateToProps = ({ categoryMeetingReducer }) => {
//     const { isLoading, error, data, status } = categoryMeetingReducer;
//     return { isLoading, error, data, status };
// };
// export default connect(mapStateToProps, { fetchCategoryMeeting, categoryMeetingStatus })(CategoryMeetingComponent);

export default connect(
    state => {
        return {
            listData: state.categoryMeetingReducer,
            isLoading: state.categoryMeetingReducer.isLoading,
            error: state.categoryMeetingReducer.error,
            status: state.categoryMeetingReducer.status
        }
    },
    dispatch => {
        return {
            fetchCategoryMeeting: (user_id) => dispatch(fetchCategoryMeeting(user_id)),
            categoryMeetingStatus: (index) => dispatch(categoryMeetingStatus(index))
        }
    }
)(CategoryMeetingComponent);