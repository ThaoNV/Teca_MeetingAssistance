
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    Image,
    Text,
    FlatList,
    View
} from 'react-native';
import MeetingTimeLineList from '../containers/MeetingTimeLineList';
import { getMeetingTimeLineSuccess, fetchMeetingTimeLine, meetingCalendarTimeLine, meetingTimeLineCheckCalendar, meetingTimeLineUnCheckCalendar, meetingTimeLineStatus, meetingTimeLineNote } from '../actions/MeetingTimeLineAction';
import { connect } from 'react-redux';
import RNCalendarEvents from 'react-native-calendar-events';
import CheckBox from 'react-native-check-box'


export class MeetingTimeLineComponent extends Component {

    componentDidMount() {
        console.log(this.props);
        let dataList = this.props.navigation.state.params.item.timelines.timelines;
        dataList = dataList.filter(x => x.date.indexOf(this.props.tabLabel) !== -1);
        const data = dataList[0].items;
        this.props.getMeetingTimeLineSuccess(data);
        // this.props.fetchMeetingTimeLine(this.props.navigation.state.params.item.id, this.props.navigation.state.params.user_id);

    }

    onScrollBeginDrag = (evt) => {
        const { onScrollBeginDrag } = this.props;
        onScrollBeginDrag(evt);

    }

    onScroll = (evt) => {
        const { onScroll } = this.props;
        onScroll(evt);
    }

    _onMove = (index) => {
        this.props.meetingTimeLineStatus(index);
        console.log(this.props.status);
    };



    render() {
        const { navigate } = this.props.navigation;
        const dataList = this.props.listData.data;
        const status = this.props.status;
        return (

            <FlatList
                keyExtractor={(item, index) => index.toString()}
                numColumns={1}
                data={dataList}
                extraData={this.props}
                renderItem={({ item, index }) => (
                    <MeetingTimeLineList
                        {...item}
                        index={index}
                        status={status}
                        navigate={this.props.navigation}
                        onMove={() => this._onMove(index)}
                        {...this.props}
                    />
                )}
            />
        );
    }
}

const styles = {
    // container: {
    //     flex: 1,
    //     padding: 5,
    //     backgroundColor: '#fff',
    // },
    // containerHead: {
    //     backgroundColor: '#333',
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     marginBottom: 10,
    //     padding: 10,
    //     height: 40
    // },
    // textHead: {
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     fontWeight: 'bold',
    //     color: 'white'
    // },
    // containerContent: {
    //     padding: 10,
    // },
    // subTitle: {
    //     flexDirection: 'column'
    // },
    // subItem: {
    //     flexDirection: 'row'
    // },
    // subDetailLeft: {
    //     width: 150
    // },
    // subDetailRight: {
    //     //paddingLeft: 30
    // },
    // subTextLeft: {
    //     color: '#333',
    //     fontWeight: 'bold'
    // },
    // button: {
    //     backgroundColor: 'white',
    //     padding: 20
    // },
    // text: {
    //     fontSize: 13,
    //     textAlign: 'center',
    //     marginTop: 10,
    //     color: 'white'
    // },
    // imageIcon: {
    //     width: 90,
    //     height: 90,
    //     backgroundColor: 'white',
    //     borderRadius: 80,
    //     shadowOffset: { width: 10, height: 10, },
    //     shadowColor: 'black',
    //     shadowOpacity: 1.0,
    //     justifyContent: 'center',
    //     alignItems: 'center'
    // },
    // imageIconChild: {
    //     width: 85,
    //     height: 85,
    //     backgroundColor: '#394a5e',
    //     borderRadius: 85,
    //     //   justifyContent: 'center',
    //     alignItems: 'center'
    // },
    // linearGradient: {
    //     flex: 1,
    //     paddingLeft: 15,
    //     paddingRight: 15,
    //     borderRadius: 5
    // },
    // buttonText: {
    //     fontSize: 18,
    //     fontFamily: 'Gill Sans',
    //     textAlign: 'center',
    //     margin: 10,
    //     color: '#ffffff',
    //     backgroundColor: 'transparent',
    // },
    // category: {
    //     flexDirection: 'row',
    //     alignItems: 'center'
    // },
    // categoryDot: {
    //     height: 5,
    //     width: 5,
    //     backgroundColor: '#bbb',
    //     borderRadius: 5
    // },
    // categoryText: {
    //     paddingLeft: 10,
    //     fontWeight: 'bold',
    // },
    // categoryContent: {
    //     borderStyle: 'solid',
    //     borderLeftWidth: 0.7,
    //     borderLeftColor: '#bbb',
    //     marginLeft: 1.9
    // },
    // categoryContentItem: {
    //     flexDirection: 'column',
    // },
    // categoryTextContent: {

    //     paddingLeft: 20,
    //     padding: 10,
    //     justifyContent: 'center',
    // },
    // categoryChildText: {
    //     paddingLeft: 10
    // },
    // inforText: {
    //     fontSize: 12,
    // }
};
// const mapStateToProps = ({ meetingTimeLineReducer }) => {
//     const { isLoading, error, data, timelines } = meetingTimeLineReducer;
//     return { isLoading, error, data, timelines };
// };
// export default connect(mapStateToProps, { fetchMeetingTimeLine })(MeetingTimeLineComponent);

export default connect(
    state => {
        return {
            listData: state.meetingTimeLineReducer,
            isLoading: state.meetingTimeLineReducer.isLoading,
            error: state.meetingTimeLineReducer.error,
            status: state.meetingTimeLineReducer.status
        }
    },
    dispatch => {
        return {
            getMeetingTimeLineSuccess: (data) => dispatch(getMeetingTimeLineSuccess(data)),
            fetchMeetingTimeLine: (id, userid, uuid) => dispatch(fetchMeetingTimeLine(id, userid, uuid)),
            meetingCalendarTimeLine: (calendar_id, status, user_id, timeline_id, uuid) => dispatch(meetingCalendarTimeLine(calendar_id, status, user_id, timeline_id, uuid)),
            meetingTimeLineCheckCalendar: (index, calendar_id) => dispatch(meetingTimeLineCheckCalendar(index, calendar_id)),
            meetingTimeLineUnCheckCalendar: (index) => dispatch(meetingTimeLineUnCheckCalendar(index)),
            meetingTimeLineStatus: (index) => dispatch(meetingTimeLineStatus(index)),
            meetingTimeLineNote: (index, user_id, timeline_id, note) => dispatch(meetingTimeLineNote(index, user_id, timeline_id, note))
        }
    }
)(MeetingTimeLineComponent);