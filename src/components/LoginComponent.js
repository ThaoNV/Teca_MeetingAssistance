
import React, { Component } from 'react';
import {
    Animated,
    Platform,
    StyleSheet,
    KeyboardAvoidingView,
    Text,
    Image,
    Easing,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    View
} from 'react-native';
import { connect } from 'react-redux';
import Spinner from '../containers/Spinner';
const Dimensions = require('Dimensions');
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

import UserInput from '../containers/UserInput';
import { loginUser, otpChanged } from '../actions/LoginAction';


export class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.buttonAnimated = new Animated.Value(0);
    }
    onOTPChange(text) {
        this.props.otpChanged(text);
        console.log(this.props);
    }
    _onPress() {
        if (this.props.showLoading) return;
        const { otp, navigation } = this.props;
        this.props.loginUser(navigation, '1', otp, '9317a44b0d27d3cc6273611d64391b8f');
    }

    render() {
        const changeWidth = this.buttonAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
        });
        return (
            <View>
                <ImageBackground style={styles.picture} source={require('../images/bg_login.png')}>
                    <View style={{
                        flex: 3,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <Image source={require('../images/logo_login.png')} style={{
                            width: 180,
                            height: 220,
                        }} />
                    </View>
                    <View style={{
                        height: 100
                    }}>
                        {/* <Text style={{
                        alignSelf: 'center',
                        alignItems: 'center',
                        color: '#fff',
                        fontSize: 25,
                        justifyContent: 'center'
                    }}>+841686836566</Text> */}
                    </View>
                    {/* <KeyboardAvoidingView behavior="padding" style={{
                    flex: 0.6,
                    alignItems: 'center',
                }}>
                    <UserInput
                        source={require('../images/171508-32.png')}
                        placeholder="Mã OTP"
                        returnKeyType={'done'}
                        value={this.props.otp}
                        onChangeText={this.onOTPChange.bind(this)}
                        autoCapitalize={'none'}
                        autoCorrect={true}
                    />

                </KeyboardAvoidingView> */}
                    <View style={styles.inputWrapper}>
                        <Image source={require('../images/171508-32.png')} style={styles.inlineImg} />
                        <TextInput
                            style={styles.input}
                            value={this.props.otp}
                            onChangeText={this.onOTPChange.bind(this)}
                            placeholderTextColor="#eee"
                            keyboardType='email-address'
                            placeholder="Mã OTP"
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={{
                        flex: 1,
                        top: -0,
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}>
                        <Animated.View style={{ width: changeWidth }}>
                            {
                                this.props.isLoading ? (
                                    <View>
                                        <Spinner visible={true} textStyle={{ color: '#FFF' }} />
                                        <TouchableOpacity
                                            style={styles.button}
                                            onPress={this._onPress.bind(this)}
                                            activeOpacity={1}>
                                            <Text style={styles.text}>XÁC NHẬN</Text>
                                        </TouchableOpacity>
                                    </View>
                                ) : (

                                        <View>
                                            <TouchableOpacity
                                                style={styles.button}
                                                onPress={this._onPress.bind(this)}
                                                activeOpacity={1}>
                                                <Text style={styles.text}>XÁC NHẬN</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                            }
                        </Animated.View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    inputWrapper: {
        // flex: 1
        marginBottom: 10
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ec1c24',
        height: MARGIN,
        borderRadius: 8,
        zIndex: 100,
    },
    image: {
        width: 24,
        height: 24,
    },
    picture: {
        width: '100%',
        height: '100%',
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    circle: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#4267b2',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
    input: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 8,
        borderColor: 'rgba(255,255,255,0.3)',
        borderWidth: 1,
        color: '#ffffff',
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                paddingBottom: 0
            },
            android: {
                paddingBottom: 6
            }
        })
    },
};
const mapStateToProps = ({ loginReducer }) => {
    const { showLoading, status, msg, otp, id } = loginReducer;
    return { showLoading, status, msg, otp, id };

};

export default connect(mapStateToProps, {
    loginUser, otpChanged
})(LoginComponent);