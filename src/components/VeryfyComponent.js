import React, { Component } from 'react';
import {
    Animated,
    Platform,
    StyleSheet,
    KeyboardAvoidingView,
    Text,
    Image,
    Alert,
    Easing,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    View
} from 'react-native';
 import Spinner from '../containers/Spinner';
import { connect } from 'react-redux';
import { emailChanged, phoneChanged, registerUser, Auth } from '../actions/RegisterAction';
const Dimensions = require('Dimensions');
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

import UserInput from '../containers/UserInput';

export class VeryfyComponent extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.showPass = this.showPass.bind(this);
        this.buttonAnimated = new Animated.Value(0);
        this.growAnimated = new Animated.Value(0);
        // this._onPress = this._onPress.bind(this);
    }

    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPhoneChange(text) {
        this.props.phoneChanged(text);
    }

    onPress() {

        const { email, phone, navigation } = this.props;
        this.props.registerUser(navigation, email, phone, '9317a44b0d27d3cc6273611d64391b8f');
    }

    _onGrow() {
        Animated.timing(this.growAnimated, {
            toValue: 1,
            duration: 200,
            easing: Easing.linear
        }).start();
    }

    showPass() {
        this.state.press === false
            ? this.setState({ showPass: false, press: true })
            : this.setState({ showPass: true, press: false });
    }

    componentDidMount() {
    }

    componentWillUnmount() {

        //clearInterval(this.state.interval);
    }

    render() {
        const changeWidth = this.buttonAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
        });
        const changeScale = this.growAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [1, MARGIN]
        });
        return (
            <View >
                <ImageBackground style={styles.picture} source={require('../images/bg_login.png')}>
                    <View style={{
                        flex: 3,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <Image source={require('../images/logo_login.png')} style={{
                            width: 180,
                            height: 220,
                        }} />
                    </View>
                    {/* <KeyboardAvoidingView behavior="padding" style={{
                    flex: 1,
                    alignItems: 'center',
                }}>
                    <UserInput
                        source={require('../images/username.png')}
                        value={this.props.email}
                        onChangeText={this.onEmailChange.bind(this)}
                        keyboardType='email-address'
                        placeholder="Email"
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        autoCorrect={true}
                    />
                    <UserInput
                        source={require('../images/171508-32.png')}
                        value={this.props.phone}
                        onChangeText={this.onPhoneChange.bind(this)}
                        placeholder="Số điện thoại"
                        returnKeyType={'done'}
                        keyboardType='numeric'
                        autoCapitalize={'none'}
                        autoCorrect={true}
                    />

                </KeyboardAvoidingView> */}
                    <View style={styles.inputWrapper}>
                        <Image source={require('../images/username.png')} style={styles.inlineImg} />
                        <TextInput
                            style={styles.input}
                            value={this.props.email}
                            onChangeText={this.onEmailChange.bind(this)}
                            placeholderTextColor="#eee"
                            keyboardType='email-address'
                            placeholder="Email"
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={styles.inputWrapper}>
                        <Image source={require('../images/171508-32.png')} style={styles.inlineImg} />
                        <TextInput
                            style={styles.input}
                            value={this.props.phone}
                            onChangeText={this.onPhoneChange.bind(this)}
                            placeholderTextColor="#eee"
                            placeholder="Số điện thoại"
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={{
                        flex: 1,
                        top: -0,
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}>
                        <Animated.View style={{ width: changeWidth }}>
                            {
                                this.props.isLoading ? (
                                    <View>
                                        <Spinner visible={true} textStyle={{ color: '#FFF' }} />
                                        <TouchableOpacity
                                            style={styles.button}
                                            onPress={this.onPress.bind(this)}
                                            activeOpacity={1}>
                                            <Text style={styles.text}>XÁC NHẬN</Text>
                                        </TouchableOpacity>
                                    </View>
                                ) : (
                                        <TouchableOpacity
                                            style={styles.button}
                                            onPress={this.onPress.bind(this)}
                                            activeOpacity={1}>
                                            <Text style={styles.text}>XÁC NHẬN</Text>
                                        </TouchableOpacity>
                                    )
                            }
                        </Animated.View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ec1c24',
        height: MARGIN,
        borderRadius: 8,
        zIndex: 100,
    },
    image: {
        width: 24,
        height: 24,
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    inputWrapper: {
        // flex: 1
        marginBottom: 10
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
    input: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 8,
        borderColor: 'rgba(255,255,255,0.3)',
        borderWidth: 1,
        color: '#ffffff',
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                paddingBottom: 0
            },
            android: {
                paddingBottom: 6
            }
        })
    },
    picture: {
        width: '100%',
        height: '100%',
    },
    circle: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#4267b2',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
};

const mapStateToProps = ({ registerReducer }) => {
    const { isLoading, press, email, phone, status, loginError, msg } = registerReducer;
    return { isLoading, press, email, phone, status, loginError, msg };

};

export default connect(mapStateToProps, {
    emailChanged, phoneChanged, registerUser, Auth
})(VeryfyComponent);
