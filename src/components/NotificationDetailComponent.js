
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    Dimensions,
    Text,
    View,
    WebView
} from 'react-native';

import { fetchNotificationDetail } from '../actions/NotificationDetailAction';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';

export class NotificationDetailComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchNotificationDetail(this.props.navigation.state.params.user_id, this.props.navigation.state.params.noti_id);
    }
    render() {
        console.log(this.props);
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.content}>
                        <Text style={styles.noti_item_title}>{this.props.listData.data.title}</Text>
                        <HTML style={styles.categoryChildText} html={`<div>${this.props.listData.data.content}</div>`} {...this.props} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const win = Dimensions.get('window');
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    content: {
        padding: 10
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    categoryChildText: {
        fontSize: 12,
    },
    noti_title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#202124',
        alignItems: 'center',
        textAlign: 'left',
        fontSize: 17
    },
    noti_item_title: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000000',
        width: win.width,
        textAlign: 'center',
    },


};

export default connect(
    state => {
        return {
            listData: state.notificationDetailReducer,
            isLoading: state.notificationDetailReducer.isLoading,
            error: state.notificationDetailReducer.error,
            status: state.notificationDetailReducer.status
        }
    },
    dispatch => {
        return {
            fetchNotificationDetail: (user_id, noti_id) => dispatch(fetchNotificationDetail(user_id, noti_id))
        }
    }
)(NotificationDetailComponent);
