
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    Text,
    View
} from 'react-native';

import { fetchSupport, SupportStatus } from '../actions/SupportAction';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';

export class SupportComponent extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.fetchSupport();
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.content}>
                        <HTML style={styles.categoryChildText} html={this.props.listData.data.help} {...this.props} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    content: {
        padding: 10
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    categoryChildText: {
        fontSize: 12,
    },
};

export default connect(
    state => {
        return {
            listData: state.supportReducer,
            isLoading: state.supportReducer.isLoading,
            error: state.supportReducer.error,
            status: state.supportReducer.status
        }
    },
    dispatch => {
        return {
            fetchSupport: () => dispatch(fetchSupport()),
            SupportStatus: (index) => dispatch(SupportStatus(index))
        }
    }
)(SupportComponent);
