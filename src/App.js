import React, { Component } from 'react';
import { YellowBox, View, AsyncStorage } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import NavigationExperimental from 'react-native-deprecated-custom-components';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
// import global from './api/global';
import getUser from './api/getUser';
import LoginLayout from './layout/LoginLayout';
import MainLayout from './layout/MainLayout';
import MenuLayout from './layout/MenuLayout';
import VeryfyLayout from './layout/VeryfyLayout';
import ProfileLayout from './layout/ProfileLayout';
import MeetingLayout from './layout/MeetingLayout';
import MeetingTimeLayout from './layout/MeetingTimeLayout';
import SupportLayout from './layout/SupportLayout';
import AboutUsLayout from './layout/AboutUsLayout';
import NotificationLayout from './layout/NotificationLayout';
import NotificationDetailLayout from './layout/NotificationDetailLayout';
import SetupLayout from './layout/SetupLayout';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { user: null };
    }

    componentDidMount() {
        this.initStart();
    }


    initStart = async () => {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value != null) {
                const data = JSON.parse(value);
                this.setState({ user: data })
            }
        } catch (error) {
            // Error retrieving data
        }

    }

    renderNavigator() {
        const StackNavigator = createStackNavigator({
            MainLayout: { screen: MainLayout },
            // VeryfyLayout: { screen: VeryfyLayout },
            // LoginLayout: { screen: LoginLayout },
            // MainLayout: { screen: MainLayout },
            ProfileLayout: { screen: ProfileLayout },
            MeetingLayout: { screen: MeetingLayout },
            MeetingTimeLayout: { screen: MeetingTimeLayout },
            SupportLayout: { screen: SupportLayout },
            AboutUsLayout: { screen: AboutUsLayout },
            NotificationLayout: { screen: NotificationLayout },
            NotificationDetailLayout: { screen: NotificationDetailLayout },
            SetupLayout: { screen: SetupLayout }
        });

        const StackNavigatorAuth = createStackNavigator({
            // VeryfyLayout: { screen: VeryfyLayout },
            // LoginLayout: { screen: LoginLayout },
            MainLayout: { screen: MainLayout },
            ProfileLayout: { screen: ProfileLayout },
            MeetingLayout: { screen: MeetingLayout },
            MeetingTimeLayout: { screen: MeetingTimeLayout },
            SupportLayout: { screen: SupportLayout },
            AboutUsLayout: { screen: AboutUsLayout },
            NotificationLayout: { screen: NotificationLayout },
            NotificationDetailLayout: { screen: NotificationDetailLayout },
            SetupLayout: { screen: SetupLayout }
        });
        const Main = this.state.user != null ? StackNavigator : StackNavigatorAuth;

        const DrawerNavigator = createDrawerNavigator(
            {
                HomeLayout: { screen: Main },
            },
            {
                contentComponent: props => <MenuLayout {...props} />
            }
        );

        const NavigatorAuth = createStackNavigator(
            {
                VeryfyLayout: { screen: VeryfyLayout },
                LoginLayout: { screen: LoginLayout },
                HomeLayout: { screen: DrawerNavigator }
            },
            {
                headerMode: 'none',
                navigationOptions: {
                    headerVisible: false,
                }
            }
        )

        const NavigatorUser = createStackNavigator(
            {
                HomeLayout: { screen: DrawerNavigator },
                VeryfyLayout: { screen: VeryfyLayout },
                LoginLayout: { screen: LoginLayout }
            },
            {
                headerMode: 'none',
                navigationOptions: {
                    headerVisible: false,
                }
            }
        )

        const MainUser = this.state.user != null ? NavigatorUser : NavigatorAuth;

        return (
            <MainUser />
        );
    }

    render() {
        return (
            this.renderNavigator()
        );
    }

}
export default App;
