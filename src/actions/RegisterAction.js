import { register } from '../api/register';
import { Alert } from 'react-native';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    EMAIL_CHANGED,
    PHONE_CHANGED,
    EMAIL_FAIL,
    PHONE_FAIL
} from './types';

export const emailChanged = (text) => {
    return (dispatch) => dispatch({
        type: EMAIL_CHANGED,
        payload: text
    });
}

export const phoneChanged = (text) => {

    return (dispatch) => dispatch({
        type: PHONE_CHANGED,
        payload: text
    });
}

export const Auth = (text) => {
    return {
        type: EMAIL_FAIL,
        payload: text
    }
}

export const registerUserApp = () => {
    return {
        type: REGISTER_USER,
        payload: text
    }
}

export const registerUserSuccess = (data) => {
    return (dispatch) => dispatch({
        type: REGISTER_USER_SUCCESS,
        payload: data
    });
}


export const registerUser = (navigation, email, phone, signature) => {

    return (dispatch) => {
        dispatch({ type: REGISTER_USER });
        if (!ValidateEmail(email)) {
            var text = '';
            if (email == '') {
                text = 'Email không được để trống!';
            } else {
                text = 'Định dạng email không chính xác!'
            }
            Alert.alert(
                'Thông báo',
                text,
                [
                    { text: 'OK' }
                ],
                { cancelable: false }
            );
            dispatch({
                type: EMAIL_FAIL
            });
        } else if (ValidatePhone(phone) != '') {
            var text = ValidatePhone(phone);
            Alert.alert(
                'Thông báo',
                text,
                [
                    { text: 'OK' }
                ],
                { cancelable: false }
            );
            dispatch({
                type: PHONE_FAIL,
                payload: text
            });
        } else {
            //   navigation.navigate('LoginLayout');
            register(email, phone, '9317a44b0d27d3cc6273611d64391b8f').then(response => {
                if(response.status == 1){
                    dispatch({
                        type: REGISTER_USER_SUCCESS,
                        payload: text
                    });
                    navigation.navigate('LoginLayout', {id: response.data.id });
                } else{
                    Alert.alert(
                        'Thông báo',
                        'Email và số điện thoại chưa tồn tại trong hệ thống!',
                        [
                            { text: 'OK' }
                        ],
                        { cancelable: false }
                    );
                    dispatch({
                        type: PHONE_FAIL,
                        payload: text
                    });
                }
            });
        }
    };
}

function ValidateEmail(email) {

    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        return true;
    }
    return false;
}

function ValidatePhone(phone) {
    var error = "";
    var stripped = phone.replace(/[\(\)\.\-\ ]/g, '');

    if (stripped == "") {
        error = "Số điện thoại không được để trống";
    } else if (isNaN(parseInt(stripped))) {
        phone = "";
        error = "Số điện thoại chứa các ký tụ không hợp lệ!";

    } else if (stripped.length > 11 || stripped.length < 5) {
        phone = "";
        error = "Số điện thoại có độ dài sai!";
    }
    return error;
}

function makeid() {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 32; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

