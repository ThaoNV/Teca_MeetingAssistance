import { veryfyOTP } from '../api/veryfyOTP';
import { saveDevice } from '../api/saveDevice';
import { saveUser } from '../api/saveUser';
import global from '../api/global';
import { Alert, AsyncStorage } from 'react-native';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    OTP_CHANGED
} from './types';

export const login = () => {
    return (dispatch) => dispatch({
        type: LOGIN_USER
    });
}

export const loginUserSuccess = (data) => {
    return (dispatch) => dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: data
    });
}

export const loginUserFail = () => {
    return (dispatch) => dispatch({
        type: LOGIN_USER_FAIL
    });
}

export const otpChanged = (text) => {
    return {
        type: OTP_CHANGED,
        payload: text
    }
}

export const loginUser = (navigation, id, otp, signature) => {
    return (dispatch) => {
        dispatch(login());
        veryfyOTP(id, otp, signature).then((data) => {
            if (data.status == 1) {
                dispatch(loginUserSuccess(data.data));
               // console.log(data);
                //saveDevice(data.data.uuid);
                saveUser(data.data);
             
               // global.onSignIn = data.data
              //  console.log(global);
                navigation.navigate('MainLayout', {data: data.data});
            } else {
                var txt = "";
                if (otp == '') {
                    txt = 'Mã OTP không được để trống';
                } else{
                    txt = 'Mã OTP sai xin hãy nhập lại!';
                }
                Alert.alert(
                    'Thông báo',
                    txt,
                    [
                        { text: 'OK' }
                    ],
                    { cancelable: false }
                );
                dispatch(loginUserFail())
            }
        }).catch((error) => { dispatch(loginUserFail()) })
    };
}
