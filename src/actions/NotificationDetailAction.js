import { notificationItem } from '../api/notification';
import {
    NOTIFICATION_DETAIL_FETCHING,
    NOTIFICATION_DETAIL_FETCHING_SUCCESS,
    NOTIFICATION_DETAIL_FETCHING_FAIL,
    NOTIFICATION_DETAIL_STATUS
} from '../actions/types'

export const getNotificationDetail = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_DETAIL_FETCHING,
    });
}

export const getNotificationDetailSuccess = (data) => {
 
    return (dispatch) => dispatch({
        type: NOTIFICATION_DETAIL_FETCHING_SUCCESS,
        payload: data
    });
}

export const getNotificationDetailFail = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_DETAIL_FETCHING_FAIL,
    });
}

export const NotificationDetailStatus = (index)=>{
    return (dispatch) => dispatch({
        type: NOTIFICATION_DETAIL_STATUS,
        atIndex: index,
    });
}


export const fetchNotificationDetail = (userid, notificationid) => {
    return (dispatch) => {
        dispatch(getNotificationDetail());
        notificationItem(userid, notificationid).then((data) => {
            console.log(data);
            dispatch(getNotificationDetailSuccess(data.data))
        }).catch((error) => {
            dispatch(getNotificationDetailFail())
        })
    };
};

export default fetchNotificationDetail;