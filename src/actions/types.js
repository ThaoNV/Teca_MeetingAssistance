//Login Actions
export const LOGIN_USER = 'login_user';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';

// Register Actions
export const REGISTER_USER = 'register_user';
export const REGISTER_USER_SUCCESS = 'resgister_user_success';
export const REGISTER_USER_FAIL = 'register_user_fail';

// Login/Register Actions
export const EMAIL_CHANGED = 'email_changed';
export const PHONE_CHANGED = 'phone_changed';
export const OTP_CHANGED = 'otp_changed';
export const EMAIL_FAIL = 'email_fail';
export const PHONE_FAIL = 'phone_fail';
export const OTP_FAIL = 'otp_fail';

// Category Meeting
export const CATEGORY_MEETING_FETCHING = 'category_meeting_fetching';
export const CATEGORY_MEETING_FETCHING_SUCCESS = 'category_meeting_fetching_success';
export const CATEGORY_MEETING_FETCHING_FAIL = 'category_meeting_fetching_fail';
export const CATEGORY_MEETING_STATUS = 'category_meeting_status';
// Meeting
export const MEETING_FETCHING = 'meeting_fetching';
export const MEETING_FETCHING_SUCCESS = 'meeting_fetching_success';
export const MEETING_FETCHING_FAIL = 'meeting_fetching_fail';


// Meeting time line
export const MEETINGTIMELINE_FETCHING = 'meetingtimeline_fetching';
export const MEETINGTIMELINE_FETCHING_SUCCESS = 'meetingtimeline_fetching_success';
export const MEETINGTIMELINE_FETCHING_FAIL = 'meetingtimeline_fetching_fail';
export const MEETINGTIMELINE_STATUS = 'meetingtimeline_status';
export const MEETINGTIMELINE_NOTE_SUCCESS = 'meetingtimeline_note_success';
export const MEETINGTIMELINE_NOTE_FAIL = 'meetingtimeline_note_success';
// Meeting check Calendar
export const MEETING_CHECK_CALENDAR = 'meeting_check_calendar';
export const MEETING_UNCHECK_CALENDAR = 'meeting_uncheck_calendar';
// Meeting TimeLine check Calendar
export const MEETINGTIMELINE_CHECK_CALENDAR = 'meetingtimeline_check_calendar';
export const MEETINGTIMELINE_UNCHECK_CALENDAR = 'meetingtimeline_uncheck_calendar';


// Notifications
export const NOTIFICATION = 'notification';
export const NOTIFICATION_FETCHING = 'notification_fetching';
export const NOTIFICATION_FETCHING_SUCCESS = 'notification_fetching_success';
export const NOTIFICATION_FETCHING_FAIL = 'notification_fetching_fail';
export const NOTIFICATION_FETCHING_ITEM = 'notification_fetching_item';
export const NOTIFICATION_FETCHING_ITEM_SUCCESS = 'notification_fetching_item_success';
export const NOTIFICATION_FETCHING_ITEM_FAIL = 'notification_fetching_item_fail';
export const NOTIFICATION_ITEM_CLOSE = 'notification_item_close';

// Notification Detail
export const NOTIFICATION_DETAIL_FETCHING = 'about_us_fetching';
export const NOTIFICATION_DETAIL_FETCHING_SUCCESS = 'about_us_fetching_success';
export const NOTIFICATION_DETAIL_FETCHING_FAIL = 'about_us_fetching_fail';
export const NOTIFICATION_DETAIL_STATUS = 'about_us_status';

// About us
export const ABOUT_US_FETCHING = 'about_us_fetching';
export const ABOUT_US_FETCHING_SUCCESS = 'about_us_fetching_success';
export const ABOUT_US_FETCHING_FAIL = 'about_us_fetching_fail';
export const ABOUT_US_STATUS = 'about_us_status';

// Support
export const SUPPORT_FETCHING = 'support_fetching';
export const SUPPORT_FETCHING_SUCCESS = 'support_fetching_success';
export const SUPPORT_FETCHING_FAIL = 'support_fetching_fail';
export const SUPPORT_STATUS = 'support_status';