import { aboutUs } from '../api/aboutUs';
import {
    ABOUT_US_FETCHING,
    ABOUT_US_FETCHING_SUCCESS,
    ABOUT_US_FETCHING_FAIL,
    ABOUT_US_STATUS
} from '../actions/types'

export const getAboutUs = () => {
    return (dispatch) => dispatch({
        type: ABOUT_US_FETCHING,
    });
}

export const getAboutUsSuccess = (data) => {
 
    return (dispatch) => dispatch({
        type: ABOUT_US_FETCHING_SUCCESS,
        payload: data
    });
}

export const getAboutUsFail = () => {
    return (dispatch) => dispatch({
        type: ABOUT_US_FETCHING_FAIL,
    });
}

export const AboutUsStatus = (index)=>{
    return (dispatch) => dispatch({
        type: ABOUT_US_STATUS,
        atIndex: index,
    });
}


export const fetchAboutUs = () => {
    return (dispatch) => {
        dispatch(getAboutUs());
        aboutUs().then((data)=>{
            console.log(data);
            dispatch(getAboutUsSuccess(data.data))
        })
        .catch((error)=>{dispatch(getAboutUsFail())})
    }
}

export default fetchAboutUs;