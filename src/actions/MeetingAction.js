import { meeting, meetingCalendar } from '../api/meeting';
import {
    MEETING_FETCHING,
    MEETING_FETCHING_SUCCESS,
    MEETING_FETCHING_FAIL,
    MEETING_CHECK_CALENDAR,
    MEETING_UNCHECK_CALENDAR
} from '../actions/types'

export const getMeeting = () => {
    return (dispatch) => dispatch({
        type: MEETING_FETCHING,
    });
}

export const getMeetingSuccess = (data) => {

    return (dispatch) => dispatch({
        type: MEETING_FETCHING_SUCCESS,
        payload: data
    });
}

export const getMeetingFail = () => {
    return (dispatch) => dispatch({
        type: MEETING_FETCHING_FAIL,
    });
}

export const meetingCheckCalendar=(index, calendar_id)=>{
    return{
        type: MEETING_CHECK_CALENDAR,
        atIndex: index,
        calendar_id: calendar_id
    }
}

export const meetingUnCheckCalendar=(index)=>{
    return{
        type: MEETING_UNCHECK_CALENDAR,
        atIndex: index
    }
}


export const fetchMeeting = (type, userid, uuid) => {
    return (dispatch) => {
        dispatch(getMeeting());
        meeting(type, userid, uuid).then((data) => {
            console.log(data);
            dispatch(getMeetingSuccess(data.data))
        })
            .catch((error) => { dispatch(getMeetingFail()) })
    }
}

export const meetingCalendarMeeting = (calendar_id, status, user_id, meeting_id, uuid) => {
  
    return (dispatch) => {
        meetingCalendar(calendar_id, status, user_id, meeting_id, uuid).then((data) => {
            console.log(data);
        }).catch((error) => { })
    }
}

export default fetchMeeting;