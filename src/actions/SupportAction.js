import { support } from '../api/support';
import {
    SUPPORT_FETCHING,
    SUPPORT_FETCHING_SUCCESS,
    SUPPORT_FETCHING_FAIL,
    SUPPORT_STATUS
} from '../actions/types'

export const getSupport = () => {
    return (dispatch) => dispatch({
        type: SUPPORT_FETCHING,
    });
}

export const getSupportSuccess = (data) => {
 
    return (dispatch) => dispatch({
        type: SUPPORT_FETCHING_SUCCESS,
        payload: data
    });
}

export const getSupportFail = () => {
    return (dispatch) => dispatch({
        type: SUPPORT_FETCHING_FAIL,
    });
}

export const SupportStatus = (index)=>{
    return (dispatch) => dispatch({
        type: SUPPORT_STATUS,
        atIndex: index,
    });
}


export const fetchSupport = () => {
    return (dispatch) => {
        dispatch(getSupport());
        support().then((data)=>{
            console.log(data);
            dispatch(getSupportSuccess(data.data))
        })
        .catch((error)=>{dispatch(getSupportFail())})
    }
}

export default fetchSupport;