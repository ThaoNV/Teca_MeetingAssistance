import { categoryMeeting, pageCalendar } from '../api/categoryMeeting';
import {
    CATEGORY_MEETING_FETCHING,
    CATEGORY_MEETING_FETCHING_SUCCESS,
    CATEGORY_MEETING_FETCHING_FAIL,
    CATEGORY_MEETING_STATUS
} from '../actions/types'

export const getCategoryMeeting = () => {
    return (dispatch) => dispatch({
        type: CATEGORY_MEETING_FETCHING,
    });
}

export const getCategoryMeetingSuccess = (data) => {
 
    return (dispatch) => dispatch({
        type: CATEGORY_MEETING_FETCHING_SUCCESS,
        payload: data
    });
}

export const getCategoryMeetingFail = () => {
    return (dispatch) => dispatch({
        type: CATEGORY_MEETING_FETCHING_FAIL,
    });
}

export const categoryMeetingStatus = (index)=>{
    return (dispatch) => dispatch({
        type: CATEGORY_MEETING_STATUS,
        atIndex: index,
    });
}


export const fetchCategoryMeeting = (user_id) => {
    return (dispatch) => {
        dispatch(getCategoryMeeting());
        categoryMeeting(user_id).then((data)=>{
            console.log(data);
            dispatch(getCategoryMeetingSuccess(data.data))
        })
        .catch((error)=>{dispatch(getCategoryMeetingFail())})
    }
}

export default fetchCategoryMeeting;