import {
    NOTIFICATION_FETCHING,
    NOTIFICATION_FETCHING_SUCCESS,
    NOTIFICATION_FETCHING_FAIL,
    NOTIFICATION_FETCHING_ITEM,
    NOTIFICATION_FETCHING_ITEM_SUCCESS,
    NOTIFICATION_FETCHING_ITEM_FAIL,
    NOTIFICATION_ITEM_CLOSE
} from '../actions/types'

import { notification, notificationItem } from '../api/notification';

export const getNotifications = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING,
    });
};

export const getNotificationItem = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING_ITEM,
    });
};

export const getNotificationsSuccess = (data) => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING_SUCCESS,
        payload: data
    });
};

export const getNotificationItemSuccess = (data) => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING_ITEM_SUCCESS,
        payload: data
    });
};

export const getNotificationsFail = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING_FAIL,
    });
};

export const getNotificationItemFail = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_FETCHING_ITEM_FAIL,
    });
};

export const fetchNotificationItem = (userid, notificationid) => {
    return (dispatch) => {
        dispatch(getNotificationItem());
        notificationItem(userid, notificationid).then((data) => {
            console.log(data);
            dispatch(getNotificationItemSuccess(data.data))
        }).catch((error) => {
            dispatch(getNotificationItemFail())
        })
    };
};

export const fetchNotification = (userid) => {
    return (dispatch) => {
        dispatch(getNotifications());
        notification(userid).then((data) => {
            console.log(data);
            dispatch(getNotificationsSuccess(data.data))
        }).catch((error) => {
            dispatch(getNotificationsFail())
        })
    };
};

export const closeNotificationItem = () => {
    return (dispatch) => dispatch({
        type: NOTIFICATION_ITEM_CLOSE
    });
};

export default fetchNotification;