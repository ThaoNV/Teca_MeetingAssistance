import { meetingTimeLine, meetingTimeLineCalendar, meetingTimeLineUserNote } from '../api/meetingTimeLine';
import Toast from 'react-native-simple-toast';
import {
    MEETINGTIMELINE_FETCHING,
    MEETINGTIMELINE_FETCHING_SUCCESS,
    MEETINGTIMELINE_FETCHING_FAIL,
    MEETINGTIMELINE_CHECK_CALENDAR,
    MEETINGTIMELINE_UNCHECK_CALENDAR,
    MEETINGTIMELINE_STATUS,
    MEETINGTIMELINE_NOTE_SUCCESS,
    MEETINGTIMELINE_NOTE_FAIL
} from '../actions/types'

export const getMeetingTimeLine = () => {
    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_FETCHING,
    });
}

export const getMeetingTimeLineSuccess = (data) => {

    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_FETCHING_SUCCESS,
        payload: data,
        timelines: data
    });
}

export const getMeetingTimeLineFail = () => {
    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_FETCHING_FAIL,
    });
}

export const meetingTimeLineCheckCalendar = (index, calendar_id) => {
    return {
        type: MEETINGTIMELINE_CHECK_CALENDAR,
        atIndex: index,
        calendar_id: calendar_id
    }
}

export const meetingTimeLineUnCheckCalendar = (index) => {
    return {
        type: MEETINGTIMELINE_UNCHECK_CALENDAR,
        atIndex: index
    }
}

export const meetingTimeLineStatus = (index) => {
    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_STATUS,
        atIndex: index,
    });
}

export const meetingTimeNoteSuccess = (index, note) => {
    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_NOTE_SUCCESS,
        atIndex: index,
        note: note
    });
}

export const meetingTimeNoteFail = () => {
    return (dispatch) => dispatch({
        type: MEETINGTIMELINE_NOTE_FAIL,
    });
}

export const fetchMeetingTimeLine = (id, user_id, uuid) => {
    return (dispatch) => {
        dispatch(getMeetingTimeLine());
        meetingTimeLine(id, user_id, uuid).then((data) => {
            console.log(data);
            dispatch(getMeetingTimeLineSuccess(data.data))
        })
            .catch((error) => { dispatch(getMeetingTimeLineFail()) })
    }
}

export const meetingCalendarTimeLine = (calendar_id, status, user_id, timeline_id, uuid) => {
   
    meetingTimeLineCalendar(calendar_id, status, user_id, timeline_id, uuid).then((data) => {
        console.log(data);
    }).catch((error) => { })
    // return (dispatch) => {
    //     alert(calendar_id + '-' + user_id + '-' + timeline_id)
    //     meetingTimeLineCalendar(calendar_id, status, user_id, timeline_id).then((data) => {
    //         console.log(data);
    //     }).catch((error) => { })
    // }
}
export const meetingTimeLineNote = (index, user_id, timeline_id, note) => {
    meetingTimeLineUserNote(user_id, timeline_id, note).then(response => {
        if (response.status == 1) {
            Toast.showWithGravity('Ghi nhớ thành công.', Toast.LONG, Toast.CENTER);
           // dispatch(meetingTimeNoteSuccess(index, note))
        } else {
           // dispatch(meetingTimeNoteFail());
           Toast.showWithGravity('Ghi nhớ thất bại.', Toast.LONG, Toast.CENTER);
        }
    }).catch((error) => { 
        //dispatch(meetingTimeNoteFail()); 
        Toast.showWithGravity('Ghi nhớ thất bại.', Toast.LONG, Toast.CENTER);
     });
    // return (dispatch) => {
    //     console.log(index + '-' + user_id + '-' + timeline_id + '-' + note)
    //     meetingTimeLineUserNote(user_id, timeline_id, note).then(response => {
    //         if (response.status == 1) {
    //             Toast.show('Ghi nhớ thành công!');
    //             dispatch(meetingTimeNoteSuccess(index, note))
    //         } else {
    //             dispatch(meetingTimeNoteFail());
    //             Toast.show('Ghi nhớ thất bại!');
    //         }
    //     }).catch((error) => { dispatch(meetingTimeNoteFail()); Toast.show('Ghi nhớ thất bại!'); });
    // }
}
export default fetchMeetingTimeLine;