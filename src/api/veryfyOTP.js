export const veryfyOTP = (id, otp, signature) => {
    return fetch('https://meeting.vietdemo.com/api/otp',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({ id, otp, signature })
        })
        .then(res => res.json());
}

export default veryfyOTP;