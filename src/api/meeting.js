
export const meeting = (type, userid, uuid) => {
    
    const url = `https://meeting.vietdemo.com/api/meetings?type=${type}&user_id=${userid}&uuid=${uuid}&signature=9317a44b0d27d3cc6273611d64391b8f`;
    return fetch(url)
    .then(res => res.json());
};

export const meetingCalendar = (calendar_id, status, user_id, meeting_id, uuid) => {
    const url = `https://meeting.vietdemo.com/api/calendar/check?calendar_id=${calendar_id}&status=${status}&user_id=${user_id}&meeting_id=${meeting_id}&uuid=${uuid}`;
    console.log(url);
    return fetch(url)
    .then(res => res.json());
}

export default meeting;