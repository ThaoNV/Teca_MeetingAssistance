export const register = (email, phone, signature) => {
    return fetch('https://meeting.vietdemo.com/api/login',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({ email, phone, signature })
        })
        .then(res => res.json());
}

export default register;