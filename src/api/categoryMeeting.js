export const categoryMeeting = (user_id) => {
    const url = `https://meeting.vietdemo.com/api/types?user_id=${user_id}`;
    return fetch(url)
    .then(res => res.json());
};

export const pageCalendar = (user_id, uuid) => {
    console.log(uuid);
    const url = `https://meeting.vietdemo.com/api/page?user_id=${user_id}&uuid=${uuid}`;
    return fetch(url)
    .then(res => res.json());
};

export const clearCalendar = (user_id, uuid) => {
    const url = `https://meeting.vietdemo.com/api/calendar/clear?user_id=${user_id}&uuid=${uuid}`;
    return fetch(url)
    .then(res => res.json());
};

export default categoryMeeting;