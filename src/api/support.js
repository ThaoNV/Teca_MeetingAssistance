export const support = () => {
    const url = `https://meeting.vietdemo.com/api/page`;
    return fetch(url)
    .then(res => res.json());
};

export default support;