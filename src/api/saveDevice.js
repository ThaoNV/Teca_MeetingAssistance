import { AsyncStorage } from 'react-native';

export const saveDevice = async (device_id) => {
    try {
        await AsyncStorage.setItem('@device', device_id);
        return 'THANH_CONG';
    } catch (e) {
        return e;
    }
};

export default saveDevice;