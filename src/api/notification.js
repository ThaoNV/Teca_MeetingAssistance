export const notification = (userid) => {

    const url = `https://meeting.vietdemo.com/api/notifications?user_id=${userid}`;
    return fetch(url)
        .then(res => res.json());
};

export const notificationItem = (userid, notificationid) => {

    const url = `https://meeting.vietdemo.com/api/notifications/show?user_id=${userid}&notification_id=${notificationid}`;
    return fetch(url)
        .then(res => res.json());
};

export default notification;