import { AsyncStorage } from 'react-native';

export const getUser = async () => {
    try {
        const value = await AsyncStorage.getItem('@user');
        var data = JSON.parse(value)
       // console.log(data);
        if (data !== null) {
            return data;
        }
    } catch (error) {
        // Error retrieving data
    }
};

export default getUser;
