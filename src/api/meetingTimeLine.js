export const meetingTimeLine = (id, user_id, uuid) => {
    const url = `https://meeting.vietdemo.com/api/meetings/${id}/timelines?user_id=${user_id}&uuid=${uuid}`;
    return fetch(url)
    .then(res => res.json());
};

export const meetingTimeLineCalendar = (calendar_id, status, user_id, timeline_id, uuid) => {
    const url = `https://meeting.vietdemo.com/api/calendar/check?calendar_id=${calendar_id}&status=${status}&user_id=${user_id}&timeline_id=${timeline_id}&uuid=${uuid}`;
    console.log(url);
    return fetch(url)
    .then(res => res.json());
}

export const meetingTimeLineUserNote = (user_id, timeline_id, note) => {

    return fetch('https://meeting.vietdemo.com/api/timelines/note',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({ user_id, timeline_id, note })
        })
        .then(res => res.json());
}

export default meetingTimeLine;