export const meetingCalendar = (calendarId, status) => {
    return fetch('https://meeting.vietdemo.com/api/calendar/check',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({ calendarId, status })
        })
        .then(res => res.json());
}

export default veryfyOTP;