import { AsyncStorage } from 'react-native';

export const saveUser = async (data) => {
    try {
        const user = JSON.stringify(data);
        await AsyncStorage.setItem('@user', user);
    } catch (e) {
        return e;
    }
};

export default saveUser;