import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    Button,
    ImageBackground,
    Image,
    ScrollView,
    AsyncStorage,
    Switch,
    View
} from 'react-native';
import { saveUser } from '../api/saveUser';
import ContactComponent from '../components/ContactComponent';
import EvaluateComponent from '../components/EvaluateComponent';
import { NavigationActions, StackActions  } from 'react-navigation';

export default class MenuLayout extends Component {
    constructor(props) {
        super(props);
        // const us = this.props.navigation.state.routes[0].routes[0].params.data;

        this.state = { isNigth: false, user: null, user_id: null, name: '', image: null }
    }

    componentDidMount() {
        if (this.props.navigation.state.routes[0].routes[0].params != undefined) {
            const us = this.props.navigation.state.routes[0].routes[0].params.data;
            this.setState({ user: us, user_id: us.id, name: us.name, image: us.image });

        } else {
            this.initStart();
        }

    }

    initStart = async () => {

        try {
            const value = await AsyncStorage.getItem('@user');

            if (value != null) {
                const data = JSON.parse(value);
                this.setState({ user: data, user_id: data.id, name: data.name, image: data.image });
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    _handToggleSwitch = () => this.setState(state => ({
        isNigth: !state.isNigth
    }));

    _handleNotification = () => {
        const { navigate } = this.props.navigation;
        return navigate('NotificationLayout', { user_id: this.state.user_id });
    }

    _handleProfile = () => {
        const { navigate } = this.props.navigation;
        return navigate('ProfileLayout', { user: this.state.user });
    }

    _handleAboutUs = () => {
        const { navigate } = this.props.navigation;
        return navigate('AboutUsLayout');
    }

    _handleSupport = () => {
        const { navigate } = this.props.navigation;
        return navigate('SupportLayout');
    }

    _handleSingOut = () => {
        const { navigate } = this.props.navigation;
        saveUser(null);
        return this.props.navigation.dispatch(StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({
                  routeName: 'VeryfyLayout',
                }),
              ]
        }));
    }

    render() {

        const { navigate } = this.props.navigation;

        return (
            <View style={styles.container}>
                <View style={styles.top}>

                    <ImageBackground style={styles.picture_bg} source={require('../images/bg-user.png')}>
                        <View style={styles.user_imf}>
                            <View style={styles.headnavi}>
                                <View>
                                    <ImageBackground style={{ width: '100%' }}
                                        source={require('../images/user_mobile_02.png')}>
                                        {
                                            this.state.image !== null ? (
                                                <Image style={{ width: 100, height: 100, borderRadius: 50 }}
                                                    source={{ uri: `${this.state.image}` }} />
                                            ) : (
                                                    <Image style={{ width: 100, height: 100 }}
                                                        source={require('../images/user_mobile_02.png')} />
                                                )
                                        }

                                    </ImageBackground>
                                </View>
                                <View style={styles.imfuser}>
                                    <Text style={styles.text}>Đại biểu {this.state.name}</Text>
                                    <TouchableOpacity onPress={() => {
                                        this._handleSingOut();
                                    }}>
                                        <Text style={[styles.text, {}]}>Đăng xuất</Text>
                                    </TouchableOpacity>
                                </View>
                                {/* <View style={styles.imageIcon}>
                                 <View style={styles.imageIconChild}>
                                 <Image source={require('../images/useravatar.png')} />
                                 </View>
                                 </View>

                                 <Text style={styles.text}>Chào mừng đại biểu: Nguyễn Văn A</Text>
                                 <View style={{ flexDirection: 'row' }}>
                                 <TouchableOpacity onPress={this._onPressSingIn}>
                                 <Text style={{ color: '#b84b72', marginBottom: 10 }}>Đăng xuất</Text>
                                 </TouchableOpacity>
                                 </View> */}
                            </View>
                        </View>
                    </ImageBackground>
                </View>
                <ScrollView>
                    <View style={styles.button}>

                        <View style={{ flexDirection: 'column' }}>
                            {/* category */}
                            <View style={styles.category}>
                                <View style={styles.categoryDot}>
                                </View>
                                <Text style={styles.categoryText}>Chức năng</Text>
                            </View>
                            {/* content */}
                            <View style={styles.categoryContent}>
                                {/* content items */}
                                <View style={styles.categoryContentItem}>
                                    <TouchableOpacity style={styles.categoryTextContent} onPress={() => {
                                        this._handleNotification();
                                    }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../images/bell.png')} />
                                            <Text style={styles.categoryChildText}>Thông báo</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.categoryTextContent} onPress={() => {
                                        this._handleProfile();
                                    }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../images/documentlist.png')} />
                                            <Text style={styles.categoryChildText}>Tài khoản</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.categoryTextContent} onPress={() => {
                                        this._handleAboutUs();
                                    }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../images/clipsave.png')} />
                                            <Text style={styles.categoryChildText}>Về chúng tôi</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.categoryTextContent} onPress={() => {
                                        this._handleSupport();
                                    }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../images/commentedit.png')} />
                                            <Text style={styles.categoryChildText}>Trợ giúp</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* category */}
                            <View style={styles.category}>
                                <View style={styles.categoryDot}>
                                </View>
                                <Text style={styles.categoryText}>Liên hệ</Text>
                            </View>
                            {/* content */}
                            <View style={styles.categoryContent}>
                                {/* content items */}
                                <View style={styles.categoryContentItem}>
                                    <View style={styles.categoryTextContent}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image
                                                source={require('../images/commonemailenvelopemailoutlinestroke.png')} />
                                            <View style={{ flexDirection: 'column' }}>
                                                {/* <Text style={styles.categoryChildText}>Góp ý đánh giá ứng dụng</Text> */}
                                                {/* <Text style={[styles.categoryChildText, styles.inforText]}>Mời bạn góp ý
                                                    giúp chúng tôi hoàn thiện tốt hơn.</Text> */}
                                                <EvaluateComponent />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.categoryTextContent}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image source={require('../images/phone.png')} />
                                            <View style={{ flexDirection: 'column' }}>
                                                <ContactComponent />
                                                {/* <Text style={styles.categoryChildText}>Thông tin trụ sở</Text>
                                                <Text style={[styles.categoryChildText, styles.inforText]}>Ngõ 15, Duy
                                                    Tân, Hà Nội</Text>
                                                <Text
                                                    style={[styles.categoryChildText, styles.inforText]}>01686836566</Text> */}
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.menufooter}>
                    <Text style={styles.menuplace}>Cơ quan chủ quản:</Text>
                    <Text style={styles.menuplaceimf}>Quốc hội nước cộng hòa xã hội chủ nghĩa Việt Nam</Text>
                    <Text style={styles.menuplaceimf}>Giấy phép số 18/GP-TTĐT ngày 12/03/2015</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    top: {
        backgroundColor: '#394a5e',
    },
    button: {
        backgroundColor: 'white',
        padding: 20
    },
    picture_bg: {
        width: '100%',
        // height: '100%',
    },
    text: {
        fontSize: 13,
        textAlign: 'left',
        marginTop: 10,
        color: 'white',
        fontWeight: 'bold'
    },
    imageIcon: {
        width: 90,
        height: 90,
        backgroundColor: 'white',
        borderRadius: 80,
        shadowOffset: { width: 10, height: 10, },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageIconChild: {
        width: 85,
        height: 85,
        backgroundColor: '#394a5e',
        borderRadius: 85,
        //   justifyContent: 'center',
        alignItems: 'center'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    category: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imfuser: {
        padding: 5,
        flex: 1
    },
    headnavi: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5
    },
    categoryDot: {
        height: 5,
        width: 5,
        backgroundColor: '#bbb',
        borderRadius: 5
    },
    categoryText: {
        fontSize: 15,
        paddingLeft: 10,
        fontWeight: 'bold',
    },
    categoryContent: {
        borderStyle: 'solid',
        borderLeftWidth: 0.7,
        borderLeftColor: '#bbb',
        marginLeft: 1.9
    },
    categoryContentItem: {
        flexDirection: 'column',
    },
    categoryTextContent: {

        paddingLeft: 20,
        padding: 10,
        justifyContent: 'center',
    },
    categoryChildText: {
        paddingLeft: 10,
        fontSize: 15
    },
    inforText: {
        fontSize: 12,
    },
    menufooter: {
        padding: 10,
        backgroundColor: '#d0d2d3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuplace: {
        fontSize: 11,
        fontWeight: 'bold'
    },
    menuplaceimf: {
        fontSize: 11
    },
    user_imf: {
        ...Platform.select({
            ios: {
                paddingTop: 20,
                paddingLeft: 5,
                paddingRight: 5,
                paddingBottom: 5
            },
            android: {
                padding: 5
            }
        })
    }
};
