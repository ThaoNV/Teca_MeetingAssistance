

import React, { Component } from 'react';
import {
    Animated,
    Platform,
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import VeryfyComponent from '../components/VeryfyComponent';
import OfflineNotice from '../containers/OfflineNotice';

export default class VeryfyLayout extends Component {
    constructor(props){
        super(props);
    }
    static navigationOptions = ({ navigation }) => ({
        header: null,
    });
    render() {
        return (
            <KeyboardAwareView animated={true}>
                <OfflineNotice />
                <VeryfyComponent {...this.props} />
            </KeyboardAwareView>
        );
    }
}