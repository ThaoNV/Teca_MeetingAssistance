
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import SupportComponent from '../components/SupportComponent';

export default class SupportLayout extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <Text style={styles.headerTitle}>Trợ giúp</Text>
    ),
    headerStyle: {
      backgroundColor: '#a01f1a',
    },
    headerTintColor: 'white',
  });
  render() {
    return (
      <View style={styles.container}>
        <SupportComponent {...this.props} />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  headerTitle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    ...Platform.select({
      ios: {
        paddingLeft: 0
      },
      android: {
        paddingLeft: 60
      }
    })
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
};
