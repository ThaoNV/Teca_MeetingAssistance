
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Animated,
    ScrollView,
    Image,
    TouchableOpacity,
    WebView,
    Easing,
    View
} from 'react-native';
import Modal from 'react-native-modal';
import HTML from 'react-native-render-html';
const Dimensions = require('Dimensions');
const HEIGHT_DIVICE = Dimensions.get('window').height;
const WIDTH_DIVICE = Dimensions.get('window').width;

import ScrollableTabView, { ScrollableTabBar, FacebookTabBar } from 'react-native-scrollable-tab-view';

const NAVBAR_HEIGHT = 64;
const STATUS_BAR_HEIGHT = Platform.select({ ios: 20, android: 24 });

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

import MeetingDetailLayout from './MeetingDetailLayout';

export default class MeetingTimeLayout extends Component {

    constructor(props) {
        super(props);
        let ls = [];
        ls = this.props.navigation.state.params.item.timelines.timelines;
        this.state = {
            listData: ls,
            data: this.props.navigation.state.params.item,
            listY: [0],
            yy: null,
            scrollY: null,
            marginTop: new Animated.Value(0),
            visibleModal: null,
        }
        console.log(this.state);

    }

    componentDidMount() {
        this.setState({ visibleModal: 2 })
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <Text style={styles.headerTitle}>{navigation.state.params.item.name.length < 20 ? navigation.state.params.item.name : navigation.state.params.item.name.slice(0, 22) + '…'}</Text>
        ),
        headerStyle: {
            backgroundColor: '#a01f1a'
        },
        headerTintColor: 'white',
    });

    onPress = (event) => {
        let listY = [0];
        const { locationY } = event.nativeEvent;
        listY.push(locationY);
        console.log(listY[listY.length - 2] + ' - ' + listY[listY.length]);
        this.setState({ yy: locationY });
    }

    onTouchMove = (event) => {
        const { locationY } = event.nativeEvent;
        const { yy } = this.state;
        console.log(yy + ' - ' + locationY);
    }

    onRelease = (event) => {

    }

    onScrollBeginDrag = (evt) => {
        const { y } = evt.nativeEvent.contentOffset;
        this.setState({ yy: y });

    }

    onScroll = (evt) => {
        Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.marginTop } } }],
            { useNativeDriver: true },
        )

        console.log(this.state);
        // console.log(evt.nativeEvent.velocity.y);
        // const { scrollY } = this.state;
        // const { yy } = this.state;
        // const { y } = evt.nativeEvent.contentOffset;
        // //console.log(yy + ' - ' + y);
        // this.setState({ listY: [...this.state.listY, y] });
        // const { listY } = this.state;
        // console.log(listY[listY.length - 2] + ' - ' + listY[listY.length - 1]);
        // if (listY[listY.length - 2] <= listY[listY.length - 1]) {
        //     const marginTop = scrollY - 10;
        //     if (marginTop >= - 50) {
        //         this.setState({ scrollY: marginTop });
        //         this.setState({ marginTop });
        //     }
        // } else {
        //     console.log('qrewhdghvcbdfytryyryutruytuytuytu')
        //     //  console.log(scrollY);
        //     const marginTop = scrollY + 10;
        //     if (marginTop <= 0) {
        //         this.setState({ scrollY: marginTop });
        //         this.setState({ marginTop });
        //     }
        // }
    }

    render() {
        const { navigate } = this.props.navigation;
        let { marginTop } = this.state;
        const headerHeight = this.state.marginTop.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });
        console.log(this.props.navigation.state.params.item);
        return (
            <View style={styles.container}
            // onStartShouldSetResponder={() => true}
            // onMoveShouldSetResponder={() => true}
            //  onResponderGrant={this.onPress.bind(this)}
            //  onTouchMove={this.onTouchMove.bind(this)}
            //  onResponderRelease={this.onRelease.bind(this)}
            >
                <Modal
                    style={styles.styleModal}
                    isVisible={this.state.visibleModal === 2}
                    onBackdropPress={() => { this.setState({ visibleModal: null }) }}
                    onRequestClose={() => { this.setState({ visibleModal: null }) }}
                >
                    <View >

                        <View style={styles.modalContent}>
                            <ScrollView>
                                <View style={styles.containerContent}>
                                    <View style={styles.subTitle}>
                                        <View style={styles.subItem}>
                                            <View style={styles.subDetailLeft}>
                                                <Text style={styles.subTextLeft}>Thời gian : </Text>
                                            </View>
                                            <View style={styles.subDetailRight}>
                                                <Text style={styles.subTextRight}>{this.state.data.time}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.subItem}>
                                            <View style={styles.subDetailLeft}>
                                                <Text style={styles.subTextLeft}>Địa điểm : </Text>
                                            </View>
                                            <View style={styles.subDetailRight}>
                                                <Text style={styles.subTextRight}>{this.state.data.location}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.subItem}>
                                            <View style={styles.subDetailLeft}>
                                                <Text style={styles.subTextLeft}>ĐB tham dự : </Text>
                                            </View>
                                            <View style={styles.subDetailRight}>
                                                <Text style={styles.subTextRight}>{this.state.data.members_count}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.paddingTopContent}>
                                            <Text style={styles.subTextLeft}>Nội dung :</Text>
                                            <View style={styles.textContent}>
                                                <HTML style={styles.inforText} html={this.state.data.description} {...this.props} />
                                                {/* <Text style={styles.textContent}>{this.state.data.description}</Text> */}
                                            </View>
                                            <View style={{ height: 400, width: '100%' }}>
                                                <WebView
                                                    style={{ width: '100%' }}
                                                    source={{ html: `<html><body>${this.state.data.google_map}</body></html>` }}
                                                />
                                            </View>
                                            {/* {
                                                this.state.data.google_map != null ? (
                                                    <View style={{}}>
                                                        <Image
                                                            style={{ width: WIDTH_DIVICE - 60, height: 350 }}
                                                            source={{ uri: `${this.state.data.google_map}` }} />
                                                    </View>
                                                ) : (
                                                        <View></View>
                                                    )
                                            } */}

                                        </View>


                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.buttonClose}>
                            <TouchableOpacity onPress={() => { this.setState({ visibleModal: null }) }}>
                                <View>

                                    <Image style={{ width: 25, height: 25 }} source={require('../images/if_18_Close_106227.png')} />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Animated.View style={[styles.containerAnimation, { transform: [{ translateY: 0 }] }]}>
                    <ScrollableTabView
                        initialPage={0}
                        tabBarTextStyle={
                            fontSize = 14
                        }
                        renderTabBar={() => <ScrollableTabBar activeTextColor="#ED1C24" backgroundColor="#f1f1f1" underlineStyle={{ backgroundColor: '#ED1C24', height: 0 }} />}
                        scrollWithoutAnimation={true}>
                        {
                            this.state.listData.map((item, i) => {
                                return (
                                    <MeetingDetailLayout tabLabel={item.date} {...this.props} />
                                )
                            })
                        }
                        {/* <MeetingDetailLayout tabLabel="03/10" navigator={this.props.navigation} {...this.props} onScroll={(evt) => this.onScroll(evt)} onScrollBeginDrag={(evt) => this.onScrollBeginDrag(evt)} />
                        <MeetingDetailLayout tabLabel="04/10" {...this.props} onScroll={(evt) => this.onScroll(evt)} />
                        <MeetingDetailLayout tabLabel="05/10" {...this.props} />
                        <MeetingDetailLayout tabLabel="06/10" {...this.props} />
                        <MeetingDetailLayout tabLabel="07/10" {...this.props} />
                        <MeetingDetailLayout tabLabel="08/10" {...this.props} />
                        <MeetingDetailLayout tabLabel="09/10" {...this.props} />
                        <MeetingDetailLayout tabLabel="10/10" {...this.props} /> */}
                        {/* <MeetingMemberLayout tabLabel="Đại biểu" {...this.props} /> */}
                        {/* <MeetingSuportLayout tabLabel="Trợ giúp" {...this.props} /> */}
                    </ScrollableTabView>
                </Animated.View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        //  marginTop: -20
    },
    containerAnimation: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        //  marginTop: -20
    },
    headerTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        ...Platform.select({
            ios: {
                paddingLeft: 0
            },
            android: {
                paddingLeft: 60
            }
        })
    },
    button: {
        backgroundColor: 'lightblue',
        // padding: 12,
        //  margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent: {
        backgroundColor: 'white',

        //   justifyContent: 'center',
        //    alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: HEIGHT_DIVICE - 75
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    buttonClose: {
        position: 'absolute',
        backgroundColor: '#fff',
        borderRadius: 15,
        padding: 2,
        top: -15,
        right: -10,
        zIndex: 999
    },
    styleModal: {
        flex: 1,
        //  position: 'relative',
        padding: 20,
        zIndex: 0
    },
    containerHead: {
        backgroundColor: '#333',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        padding: 10,
        height: 40
    },
    textHead: {
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white'
    },
    textContent: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerContent: {
        paddingTop: 20,
        padding: 10,
    },
    paddingCT: {
        paddingLeft: 10,
        paddingRight: 10
    },
    subTitle: {
        flexDirection: 'column'
    },
    subItem: {
        flexDirection: 'row'
    },
    subDetailLeft: {
        width: 100,
    },
    subDetailRight: {
        flex: 1,
        fontSize: 15
        //paddingLeft: 30
    },
    subTextLeft: {
        fontSize: 15,
        color: '#333',
        fontWeight: 'bold'
    },
    button: {
        backgroundColor: 'white',
        padding: 20
    },
    text: {
        fontSize: 13,
        textAlign: 'center',
        marginTop: 10,
        color: 'white'
    },
    imageIcon: {
        width: 90,
        height: 90,
        backgroundColor: 'white',
        borderRadius: 80,
        shadowOffset: { width: 10, height: 10, },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageIconChild: {
        width: 85,
        height: 85,
        backgroundColor: '#394a5e',
        borderRadius: 85,
        //   justifyContent: 'center',
        alignItems: 'center'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    category: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paddingTopContent: {
        paddingTop: 5
    },
    categoryDot: {
        height: 5,
        width: 5,
        backgroundColor: '#bbb',
        borderRadius: 5
    },
    categoryText: {
        paddingLeft: 10,
        fontWeight: 'bold',
    },
    categoryContent: {
        borderStyle: 'solid',
        borderLeftWidth: 0.7,
        borderLeftColor: '#bbb',
        marginLeft: 1.9
    },
    categoryContentItem: {
        flexDirection: 'column',
    },
    categoryTextContent: {

        paddingLeft: 20,
        padding: 10,
        justifyContent: 'center',
    },
    categoryChildText: {
        paddingLeft: 10
    },
    inforText: {
        fontSize: 12,
    }

};
