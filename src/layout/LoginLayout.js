
import React, { Component } from 'react';
import {
    Animated,
    Platform,
    Text
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import LoginComponent from '../components/LoginComponent';


export default class LoginLayout extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Xác nhận mã OTP',
            headerTitle: (
                <Text style={styles.headerTitle}>Xác nhận mã OTP</Text>
            ),
            headerStyle: {
                backgroundColor: '#a01f1a'
            },
            headerTintColor: 'white',
        }
    };

    render() {
        return (
            <KeyboardAwareView animated={true}>
                <LoginComponent  {...this.props} />
            </KeyboardAwareView>
        );
    }


}

const styles = {
    headerTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        ...Platform.select({
            ios: {
                paddingLeft: 0
            },
            android: {
                paddingLeft: 60
            }
        })
    },
};


