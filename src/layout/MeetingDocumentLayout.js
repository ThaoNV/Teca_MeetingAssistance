
import React, { Component } from 'react';

import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  FlatList,
  ScrollView,
  Image
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import Card from '../containers/Card';
import CardSection from '../containers/CardSection';
import CardItemView from '../containers/CardItemView';
const dirs = RNFetchBlob.fs.dirs;
const android = RNFetchBlob.android;

export default class MeetingDocumentLayout extends Component {
  constructor(props) {
    super(props)
    let dataList = this.props.navigation.state.params.item.data;
    dataList = dataList.filter(x => x.date.indexOf(this.props.tabLabel) !== -1);

    const data = dataList[0].documents;
    console.log(data);
    this.state = {
      data: data
    };
  }
  _onPressItem = (item) => {
    var fileName = item.url.split('/');
    RNFetchBlob.config({
      addAndroidDownloads: {
        title: item.name,
        useDownloadManager: true,
        mediaScannable: true,
        notification: true,
        description: 'File downloaded by download manager.',
        path: `${dirs.DownloadDir}/${fileName[fileName.length-1]}`,
      },
    })
      .fetch('GET', item.url)
      .then((res) => {
        this.setState({ path: res.path() });
      })
      .catch((err) => console.log(err));
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.servicesStyle}>
        <Card>
          <CardItemView >
            <View style={styles.card}>
              <View style={styles.thumbnaiContainerlStyleDow}>
                <Image
                  style={styles.imageDowload}
                  source={require('../images/icons8-document-80.png')} />
              </View>
              <View style={styles.textContentStyle}>
                <Text style={styles.textStyle}>
                  {item.name}
                </Text>
                {/* <Text style={styles.textStyleChild}>File size: 30K</Text>
                <Text style={styles.textStyleChild}>Loại file: PDF</Text> */}
              </View>
              <View style={styles.thumbnaiContainerlStyleDow} >
                <TouchableOpacity onPress={() => this._onPressItem(item)} >
                  <Image
                    style={styles.imageDowload}
                    source={require('../images/icons8-installing-updates-filled-100.png')} />
                </TouchableOpacity>
              </View>
            </View>
          </CardItemView>
        </Card>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#e9ebee',
  },
  contentStyle: {
    flex: 1,
    flexDirection: 'column',

  },
  profileStyle: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 20 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  profileImageStyle: {
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageDowload: {
    width: 25,
    height: 25,
  },
  thumbnailStyle: {
    width: 5,
    height: 5,
  },
  textContentStyle: {
    flex: 1,
    marginLeft: 5

  },
  textStyle: {
    //fontWeight: 'bold',
    textAlign: 'left',
    alignItems: 'center',
    width: '100%',
    fontSize: 14
  },
  textStyleChild: {
    fontSize: 10
  },
  profileContentStyle: {
    flex: 1,
    margin: 5,
    width: '100%'
  },
  servicesStyle: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',

  },
  scroollStyle: {
    //  flex:1
    // paddingVertical: 10
  },
  thumbnaiContainerlStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  thumbnailStyle: {

    width: 45,
  },
  imageForHomeHeader: {
    width: 45,
    height: 45
  },
  thumbnailBottomStyle: {
    height: 50,
    width: 50,
  },
  card: {
    flexDirection: 'row',
    paddingTop: 12,
    paddingBottom: 12,
  },
  live: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4267b2',
    height: 40,
    borderRadius: 20,
    zIndex: 100,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  button_expired: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e12d2d',
    height: 40,
    borderRadius: 20,
    zIndex: 100,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  text: {
    paddingLeft: 10,
    paddingRight: 10,
    color: 'white',
    fontSize: 11,
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  thumbnaiContainerlStyleDow: {
    // width: 30,
    // height: 30
  }
};
