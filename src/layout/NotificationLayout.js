import React, { Component } from 'react';
import {
    Text,
    View,
    Platform
} from 'react-native';
import NotificationConponent from '../components/NotificationConponent'


export default class NotificationLayout extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <Text style={styles.headerTitle}>Thông báo</Text>
        ),
        headerStyle: {
            backgroundColor: '#a01f1a'
        },
        headerTintColor: 'white',
    });

    render() {
        return (
            <View style={styles.container}>
                <View style={{flex: 1}}>
                    <NotificationConponent {...this.props} />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#e9ebee',
    },
    headerTitle: {
        color: '#fff', 
        fontSize: 20, 
        fontWeight: 'bold',
       ...Platform.select({
           ios:{
             paddingLeft: 0
           },
           android:{
             paddingLeft: 60
           }
         })
     },
};
