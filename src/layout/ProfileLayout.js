
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  View
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
// import PhotoUpload from 'react-native-photo-upload'
import DatePicker from 'react-native-datepicker';
const MARGIN = 40;

export default class ProfileLayout extends Component {
  constructor(props) {
    super(props);
    var us = this.props.navigation.state.params.user;
    console.log(us);
    this.state = {
      name: us.name,
      phone: us.phone,
      email: us.email,
      ngaysinh: us.birthday,
      diachi: us.address,
      image: us.image,
      date: "2016-05-15"
    };
  }
  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <Text style={styles.headerTitle}>Thông tin cá nhân</Text>
    ),
    headerStyle: {
      backgroundColor: '#a01f1a',
    },
    headerTintColor: 'white',
  });



  render() {
    let { phone, name, email, ngaysinh, diachi, image } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.content}>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              {/* <PhotoUpload
                onPhotoSelect={avatar => {
                  if (avatar) {
                    console.log('Image base64 string: ', avatar)
                  }
                }}
              >
                <Image
                  style={{
                    paddingVertical: 30,
                    width: 150,
                    height: 150,
                    borderRadius: 75
                  }}
                  resizeMode='cover'
                  source={{
                    uri: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
                  }}
                />
              </PhotoUpload> */}
              {
                image !== null ? (
                  <Image
                    style={{
                      alignItems: 'center',
                      paddingVertical: 30,
                      width: 150,
                      height: 150,
                      borderRadius: 75
                    }}
                    resizeMode='cover'
                    source={
                      {
                        uri: `${image}`
                      }}
                  />
                ) : (
                    <Image
                      style={{
                        alignItems: 'center',
                        paddingVertical: 30,
                        width: 150,
                        height: 150,
                        borderRadius: 75
                      }}
                      resizeMode='cover'
                      source={
                        {
                          uri: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
                        }}
                    />
                  )
              }

            </View>
            <View style={styles.textImfor}>
              <Text style={styles.textF}>Họ và tên</Text>
              <Text style={styles.textA}>{name}</Text>
            </View>
            <View style={styles.textImfor}>
              <Text style={styles.textF}>Số điện thoại</Text>
              <Text style={styles.textA}>{phone}</Text>
            </View>
            <View style={styles.textImfor}>
              <Text style={styles.textF}>Email</Text>
              <Text style={styles.textA}>{email}</Text>
            </View>
            <View style={styles.textImfor}>
              <Text style={styles.textF}>Địa chỉ</Text>
              <Text style={styles.textA}>{diachi}</Text>
            </View>
            {/* <TextField
              label='Họ và tên'
              value={name}
              onChangeText={(name) => this.setState({ name })}
            />
            <TextField
              label='Số điện thoại'
              value={phone}
              onChangeText={(phone) => this.setState({ phone })}
            />
            <TextField
              label='Email'
              value={email}
              onChangeText={(email) => this.setState({ email })}
            />
            
            <TextField
              label='Địa chỉ'
              value={diachi}
              onChangeText={(diachi) => this.setState({ diachi })}
            /> */}
          </View>

        </ScrollView>
        {/* <View style={styles.btnContent}>
          <TouchableOpacity
            style={styles.button}>
            <Text style={styles.text}>Lưu</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#FFFF',

  },
  headerTitle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    ...Platform.select({
      ios: {
        paddingLeft: 0
      },
      android: {
        paddingLeft: 60
      }
    })
  },
  content: {
    padding: 10
  },
  btnContent: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ec1c24',
    height: MARGIN,
    borderRadius: 8,
    zIndex: 100,
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  textImfor: {
    flex: 1,
    flexDirection: 'column',
    paddingBottom: 20
  },
  textF:{
    fontSize: 14
  },
  textA:{
    fontSize: 17,
    color: '#333'
  }
};
