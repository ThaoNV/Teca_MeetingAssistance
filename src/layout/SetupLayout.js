
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class SetupLayout extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <Text style={{ paddingLeft: 60, color: '#fff', fontSize: 20, fontWeight: 'bold' }}>Cài đặt</Text>
        ),
        headerStyle: {
            backgroundColor: '#394a5e'
        },
        headerTintColor: 'white',
    });
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
           Đây là màn hình Cài đặt
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.jsdsdsd
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
};
