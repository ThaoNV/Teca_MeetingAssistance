
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import ScrollableTabView, { ScrollableTabBar, FacebookTabBar } from 'react-native-scrollable-tab-view';

import MeetingContentLayout from './MeetingContentLayout';
import MeetingDocumentLayout from './MeetingDocumentLayout';
import MeetingServiceLayout from './MeetingServiceLayout';
import MeetingMemberLayout from './MeetingMemberLayout';
import MeetingSuportLayout from './MeetingSuportLayout';
import MeetingTimeLineLayout from './MeetingTimeLineLayout';

export default class MeetingDetail extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  // componentDidUpdate(){
  //   alert('componentDidUpdate');
  // }

  // componentWillUpdate(){
  //   alert('componentWillUpdate');
  // }

  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <Text style={styles.headerTitle}>{navigation.state.params.item.name.length < 20 ? navigation.state.params.item.name : navigation.state.params.item.name.slice(0, 22) + '…'}</Text>
    ),
    headerStyle: {
      backgroundColor: '#a01f1a'
    },
    headerTintColor: 'white',
  });
  render() {
   
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container} >
        <ScrollableTabView
          initialPage={0}
          tabBarTextStyle={
            fontSize = 14
          }
          renderTabBar={() => <ScrollableTabBar activeTextColor="#ED1C24" underlineStyle={{ backgroundColor: '#ED1C24' }} />}
          scrollWithoutAnimation={true}>
          {/* <View style={styles.container} tabLabel='Nội dung'>
            <MeetingContentLayout tabLabel="Nội dung" navigator={this.props.navigation} {...this.props} />
          </View> */}
          <View style={styles.container} tabLabel='Nội dung'>
            <MeetingTimeLineLayout tabLabel="Nội dung" {...this.props}  />
          </View>
          <View style={styles.container} tabLabel='Đại biểu'>
            <MeetingMemberLayout tabLabel="Đại biểu" {...this.props} />
          </View>
          <View style={styles.container} tabLabel='Tài liệu'>
            <MeetingDocumentLayout tabLabel="Tài liệu" {...this.props} />
          </View>
          <View style={styles.container} tabLabel='Hỗ trợ'>
            <MeetingServiceLayout tabLabel="Hỗ trợ" {...this.props} />
          </View>
          {/* <View style={styles.container} tabLabel='Trợ giúp'>
            <MeetingServiceLayout tabLabel="Trợ giúp" {...this.props} />
          </View> */}
          {/* <MeetingMemberLayout tabLabel="Đại biểu" {...this.props} /> */}
          {/* <MeetingSuportLayout tabLabel="Trợ giúp" {...this.props} /> */}
        </ScrollableTabView>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    // backgroundColor: '#F5FCFF',

  },
  headerTitle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    ...Platform.select({
      ios: {
        paddingLeft: 0
      },
      android: {
        paddingLeft: 60
      }
    })
  },
};
