
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  View
} from 'react-native';

import Card from '../containers/Card';
import CardSection from '../containers/CardSection';
import CardItem from '../containers/CardItem';

export default class MeetingMemberLayout extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    let dataList = this.props.navigation.state.params.item.members;
    console.log(dataList);
    this.state = {
      data: dataList
    };
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.servicesStyle}>
        <Card>
          <CardItem>
            <View style={styles.card}>
              <View style={styles.thumbnaiContainerlStyle}>
                {
                  (item.image != null || item.image !== '') ? (
                    <Image
                      style={{
                        alignItems: 'center',
                        paddingVertical: 30,
                        width: 75,
                        height: 75,
                        borderRadius: 75
                      }}
                      resizeMode='cover'
                      source={{ uri: `${item.image}` }} />
                  ) : (
                      <Image
                        style={{
                          alignItems: 'center',
                          paddingVertical: 30,
                          width: 75,
                          height: 75,
                          borderRadius: 75
                        }}
                        resizeMode='cover'
                        source={require('../images/default_avatar.jpg')} />
                    )
                }
              </View>
              <View style={styles.textContentStyle}>
                <Text style={styles.textStyle}>
                  {item.name}
                </Text>
                <Text style={styles.textStyleChild}>{item.position < 35 ? item.position : item.position.slice(0, 35) + '…'}</Text>
                <View style={styles.starCalendar}>
                  <Text style={styles.textStyleChild}>{item.province != null ? item.province.name : ''}</Text>
                </View>
              </View>

            </View>
          </CardItem>
        </Card>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#e9ebee',
  },
  contentStyle: {
    flex: 1,
    flexDirection: 'column',

  },
  profileStyle: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 20 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  profileImageStyle: {
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    // width: '100%',
    // height: '100%',
  },
  textContentStyle: {
    flex: 1,
    marginLeft: 5

  },
  textStyle: {
    fontWeight: 'bold',
    textAlign: 'left',
    alignItems: 'center',
    color: '#202124',
    width: '100%',
    fontSize: 17
  },
  textStyleChild: {
    fontSize: 15
  },
  profileContentStyle: {
    flex: 1,
    margin: 5,
    width: '100%'
  },
  servicesStyle: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  scroollStyle: {
    //  flex:1
    // paddingVertical: 10
  },
  thumbnaiContainerlStyle: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
  },
  imageForHomeHeader: {
    width: 45,
    height: 45
  },
  thumbnailBottomStyle: {
    height: 50,
    width: 50,
  },
  card: {
    flexDirection: 'row',
  },
  starCalendar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  starLeft: {
    justifyContent: 'flex-end',
  },
  live: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333'
  },
  circleImage: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    //   backgroundColor: '#4267b2',
    width: 45,
    height: 45,
    // padding: 15
  },
  circleText: {
    // fontSize: 25,
    fontWeight: 'bold',
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4267b2',
    height: 40,
    borderRadius: 20,
    zIndex: 100,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  button_expired: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e12d2d',
    height: 40,
    borderRadius: 20,
    zIndex: 100,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  text: {
    paddingLeft: 10,
    paddingRight: 10,
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
};
