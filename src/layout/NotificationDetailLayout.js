
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    View
} from 'react-native';
import NotificationDetailComponent from '../components/NotificationDetailComponent';

export default class NotificationDetailLayout extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <Text style={styles.headerTitle}>{navigation.state.params.name.length < 20 ? navigation.state.params.name : navigation.state.params.name.slice(0, 22) + '…'}</Text>
        ),
        headerStyle: {
            backgroundColor: '#a01f1a'
        },
        headerTintColor: 'white',
    });

    render() {
        console.log('QEWEWEWE');
        console.log(this.props);
        return (
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    <NotificationDetailComponent {...this.props} />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#e9ebee',
    },
    headerTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        ...Platform.select({
            ios: {
                paddingLeft: 0
            },
            android: {
                paddingLeft: 60
            }
        })
    },
};

