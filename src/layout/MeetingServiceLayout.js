import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity,
  View
} from 'react-native';
import { fetchMeeting } from '../actions/MeetingAction';
import { connect } from 'react-redux';
import Card from '../containers/Card';
import CardSection from '../containers/CardSection';


export default class MeetingServiceLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: this.props.navigation.state.params.item
    };
  }
  renderItem = ({ item, index }) => {
    return (
      <View style={styles.servicesStyle}>
        <Card>
          <CardSection>
            <View style={styles.headerContentStyle}>
              <Text style={styles.headerTextStyle}>{item.title}</Text>
            </View>
          </CardSection>
          <CardSection>
            <View style={styles.viewText}>
              <Text>{item.content}</Text>
            </View>
          </CardSection>
        </Card>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data.services}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#e9ebee',
  },
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 14
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
  },
  thumbnaiContainerlStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  viewText: {

    justifyContent: 'center',
    alignItems: 'center',
  }
};
