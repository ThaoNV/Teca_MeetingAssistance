import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    Button,
    View,
    TouchableNativeFeedback,
    TextInput,
    AsyncStorage
} from 'react-native';
// import global from '../api/global';
import { pageCalendar, clearCalendar } from '../api/categoryMeeting';
import getUser from '../api/getUser';
import { createDrawerNavigator } from 'react-navigation';
import Card from '../containers/Card';
import CardSection from '../containers/CardSection';
import CardItem from '../containers/CardItem';
import CategoryMeetingComponent from '../components/CategoryMeetingComponent';
import RNCalendarEvents from 'react-native-calendar-events';
const Dimensions = require('Dimensions');
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MenuButton = (props) => {
    return (
        <TouchableOpacity style={{ paddingLeft: 20 }} onPress={() => props.navigation.openDrawer()}>
            <Image source={require('../images/menu.png')} style={{ tintColor: 'white' }} />
        </TouchableOpacity>
    )
}

export default class MainLayout extends Component {
    constructor(props) {
        super(props);

        this.state = { user: null, user_id : null };
    }

    componentDidMount() {
        if (this.props.navigation.state.params != undefined) {
            const dt = this.props.navigation.state.params.data;
            this.setState({ user: dt , user_id: dt.id });
            this._userAsync(this.props.navigation.state.params.data);
            this.ClearCalendar(this.props.navigation.state.params.data);
        } else {
            this.initStart();
        }
        //  this._userAsync(this.state.user != null ? this.state.user : this.props.navigation.state.params.data);
    }

    initStart = async () => {
        try {

            const value = await AsyncStorage.getItem('@user');

            if (value != null) {
                const data = JSON.parse(value);
                this.setState({ user: data });
                this._userAsync(this.state.user);
                this.ClearCalendar(this.state.user);
                // return;
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    ClearCalendar = (dt) => {
        pageCalendar(dt.id, dt.uuid).then((data) => {
            RNCalendarEvents.authorizeEventStore();
            var calendar = data.data.list_remove_calendar_id;
            console.log(data.data.list_remove_calendar_id);
            if (calendar.length > 0) {
                for (let i = 0; i < calendar.length; i++) {
                    RNCalendarEvents.removeEvent(calendar[i]).then(event => {
                        console.log(event);
                    })
                }
                clearCalendar(dt.id, dt.uuid).then((data)=>{
                    console.log(data);
                })
            }
        })
    }

    _userAsync = async (data) => {
        try {
            await AsyncStorage.setItem('userid', data.id.toString());
        } catch (error) {
            console.log(error);
        }

    };
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <Image style={styles.imageForHomeHeader} source={require('../images/logochinhphu.png')} />
            </View>
        ),
        headerStyle: {
            backgroundColor: '#a01f1a',
        },
        headerLeft: <MenuButton navigation={navigation} />,
        headerTintColor: 'white',
    });

    _onPressItem = (key) => {
        const { navigate } = this.props.navigation;
        switch (key) {
            case 'KHQH':
                return navigate('MeetingLayout');
            case 'PHUBTVQH':
                return navigate('MeetingLayout');
            case 'PHQHCT':
                return navigate('MeetingLayout');
            case 'PHHDUB':
                return navigate('MeetingLayout');
            case 'TG':
                return navigate('SupportLayout');
            case 'TB':
                return navigate('NotificationLayout');
            case 'CD':
                return navigate('SetupLayout');
        }
    }
    _onPressProfile = () => {
        const { navigate } = this.props.navigation;
        console.log(navigate);
        return navigate('ProfileLayout');
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.contentStyle}>
                    {/* <View style={styles.profileStyle}>
                     <View style={styles.profileImageStyle}>
                     <Image style={styles.imageStyle} source={require('../images/useravatar.png')} />
                     </View>
                     <View style={styles.profileContentStyle}>
                     <Text>Họ tên: {this.props.navigation.state.params.data.name}</Text>
                     <Text>Số điện thoại: {this.props.navigation.state.params.data.phone}</Text>
                     <Text>Ngày sinh: {this.props.navigation.state.params.data.birthday}</Text>
                     <Text>Quê quán: {this.props.navigation.state.params.data.province}</Text>
                     </View>
                     </View> */}
                    <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', }}>
                        <CategoryMeetingComponent {...this.props} {...this.state} />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#e9ebee',
    },
    contentStyle: {
        flex: 1,
        flexDirection: 'column',

    },
    profileStyle: {
        flexDirection: 'row',
        backgroundColor: '#F8F8F8',
        padding: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 20 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    profileImageStyle: {
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        // width: '100%',
        // height: '100%',
    },
    textContentStyle: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        fontSize: 13
    },
    profileContentStyle: {
        flex: 1,
        margin: 5,
        width: '100%'
    },
    servicesStyle: {
        // flex: 1,
        // flexDirection: 'row',
        // flexWrap: 'wrap',
        // justifyContent: 'flex-start',
        //height: 80,
        width: (DEVICE_WIDTH / 2) - 2,
        margin: 1
    },
    scroollStyle: {
        //  flex:1
        // paddingVertical: 10
    },
    thumbnaiContainerlStyle: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    thumbnailStyle: {
        height: 150,
        width: 150,
    },
    imageForHomeHeader: {
        width: 45,
        height: 45
    },
    thumbnailBottomStyle: {
        height: 50,
        width: 50,
    }
};

