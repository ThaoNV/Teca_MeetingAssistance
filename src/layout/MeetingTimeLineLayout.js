
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    ScrollView,
    FlatList,
    Image,
    Text,
    View
} from 'react-native';
import MeetingTimeLineComponent from '../components/MeetingTimeLineComponent';
import MeetingTimeLineList from '../containers/MeetingTimeLineList';

import { getMeetingTimeLineSuccess, fetchMeetingTimeLine, meetingCalendarTimeLine, meetingTimeLineCheckCalendar, meetingTimeLineUnCheckCalendar, meetingTimeLineStatus, meetingTimeLineNote } from '../actions/MeetingTimeLineAction';
import { connect } from 'react-redux';

export default class MeetingTimeLineLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            error: false,
            data: {},
            timelines: [],
            status: []
        }
        
    }

    componentDidMount() {
        let stus = [];
      //  console.log(this.props.navigation.state.params.item.data.timelines);
       // let dataList = this.props.navigation.state.params.item.timelines.timelines;
       let dataList = this.props.navigation.state.params.item.data;
        dataList = dataList.filter(x => x.date.indexOf(this.props.tabLabel) !== -1);
        console.log(dataList);
        const data = dataList[0].timelines;
        // this.props.getMeetingTimeLineSuccess(data);
        // this.props.fetchMeetingTimeLine(this.props.navigation.state.params.item.id, this.props.navigation.state.params.user_id);
        this.setState({
            timelines: data
        })
        for (let i = 0; i < data.length; i++) {
            if (i == 0) stus.push(true);
            stus.push(false);
        }
        this.setState({
            status: stus
        })
    }



    _onMove = (index) => {
        this.props.meetingTimeLineStatus(index);
        console.log(this.props.status);
    };

    _meetingTimeLineCheckCalendar = (index, calendar_id) =>{
        let newData = this.state.timelines;
        newData[index].is_calendar_checked = newData[index].is_calendar_checked == 0 ? 1 : 0;
        newData[index].calendar_id = calendar_id;
        this.setState({
            timelines: newData
        });
    }

    _meetingTimeLineUnCheckCalendar = (index)=>{
        let newData = this.state.timelines;
        newData[index].is_calendar_checked = 0;
        this.setState({
            timelines: newData
        })
    }

    _saveNote = (index, note) => {
        let newData = this.state.timelines;
        newData[index].note = note;
        this.setState({
            timelines: newData
        })
    }

    _timeLineStatue = (index) =>{
        let dataSet = this.state.timelines;
        let status = this.state.status;
        for (let i = 0; i < dataSet.length; i++) {
            status[i] = false
        }
        if (dataSet.length > 0) {
            status[index] = true
        }
        this.setState({
            timelines: dataSet
        })
    }

  



    render() {
        console.log(123456890);
        const { navigate } = this.props.navigation;
        const status = this.state.status;
        console.log(this.props);
        return (
            <View>
                {/* <MeetingTimeLineComponent  {...this.props} /> */}
                <FlatList
                    keyboardShouldPersistTaps='always'
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={1}
                    data={this.state.timelines}
                    extraData={this.state}
                    renderItem={({ item, index }) => (
                        // <Text>{item.title}</Text>
                        <MeetingTimeLineList
                            {...item}
                            index={index}
                            status={status}
                            navigate={this.props.navigation}
                            onMove={() => this._timeLineStatue(index)}
                            MeetingTimeLineCheckCalendar = {(calendar_id)=> this._meetingTimeLineCheckCalendar(index, calendar_id)}
                            MeetingTimeLineUnCheckCalendar = {()=> this._meetingTimeLineUnCheckCalendar(index)}
                            SaveNote = {(note)=>this._saveNote(index, note)}
                            {...this.state}
                        />
                    )}
                />
            </View>
        );
    }
}

// const mapStateToProps = (state) => {
//     return { listData: state.timelines };
// };
// export default connect(mapStateToProps)(MeetingTimeLineLayout);
// export default connect(
//     state => {
//         return {
//             listData: state.meetingTimeLineReducer,
//             isLoading: state.meetingTimeLineReducer.isLoading,
//             error: state.meetingTimeLineReducer.error,
//             status: state.meetingTimeLineReducer.status
//         }
//     },
//     dispatch => {
//         return {
//             getMeetingTimeLineSuccess: (data) => dispatch(getMeetingTimeLineSuccess(data)),
//             fetchMeetingTimeLine: (id, userid) => dispatch(fetchMeetingTimeLine(id, userid)),
//             meetingCalendarTimeLine: (calendar_id, status, user_id, timeline_id) => dispatch(meetingCalendarTimeLine(calendar_id, status, user_id, timeline_id)),
//             meetingTimeLineCheckCalendar: (index, calendar_id) => dispatch(meetingTimeLineCheckCalendar(index, calendar_id)),
//             meetingTimeLineUnCheckCalendar: (index) => dispatch(meetingTimeLineUnCheckCalendar(index)),
//             meetingTimeLineStatus: (index) => dispatch(meetingTimeLineStatus(index)),
//             meetingTimeLineNote: (index, user_id, timeline_id, note) => dispatch(meetingTimeLineNote(index, user_id, timeline_id, note))
//         }
//     }
// )(MeetingTimeLineLayout);