
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  ScrollView,
  View
} from 'react-native';


export default class MeetingContentLayout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: this.props.navigation.state.params.item
    };
    console.log(this.state);
  }

  onScrollBeginDrag = (evt) => {
    const { onScrollBeginDrag } = this.props;
    onScrollBeginDrag(evt);

  }

  onScroll = (evt) => {
    const { onScroll } = this.props;
    onScroll(evt);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          onScrollBeginDrag={event => {
            this.onScrollBeginDrag(event);
          }}
          onScroll={this.onScroll.bind(this)}
        >
          <View style={styles.containerContent}>
            <Text style={styles.subTextLeft}>{this.state.data.name}</Text>
          </View>
          <View style={styles.containerContent}>
            <View style={styles.subTitle}>
              <View style={styles.subItem}>
                <View style={styles.subDetailLeft}>
                  <Text style={styles.subTextLeft}>Thời gian: </Text>
                </View>
                <View style={styles.subDetailRight}>
                  <Text style={styles.subTextRight}>{this.state.data.time}</Text>
                </View>
              </View>
              <View style={styles.subItem}>
                <View style={styles.subDetailLeft}>
                  <Text style={styles.subTextLeft}>Địa điểm: </Text>
                </View>
                <View style={styles.subDetailRight}>
                  <Text style={styles.subTextRight}>{this.state.data.location}</Text>
                </View>
              </View>
              <View style={styles.subItem}>
                <View style={styles.subDetailLeft}>
                  <Text style={styles.subTextLeft}>ĐB tham dự: </Text>
                </View>
                <View style={styles.subDetailRight}>
                  <Text style={styles.subTextRight}>{this.state.data.members_count}</Text>
                </View>
              </View>
              <View style={styles.paddingTopContent}>
                <Text style={styles.subTextLeft}>Nội dung</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.textContent}>{this.state.data.description}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const styles = {
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: '#fff',
  },
  containerHead: {
    backgroundColor: '#333',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    padding: 10,
    height: 40
  },
  textHead: {
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'white'
  },
  textContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerContent: {
    padding: 10,
  },
  paddingCT: {
    paddingLeft: 10,
    paddingRight: 10
  },
  subTitle: {
    flexDirection: 'column'
  },
  subItem: {
    flexDirection: 'row'
  },
  subDetailLeft: {
    width: 100
  },
  subDetailRight: {
    //paddingLeft: 30
  },
  subTextLeft: {
    color: '#333',
    fontWeight: 'bold'
  },
  button: {
    backgroundColor: 'white',
    padding: 20
  },
  text: {
    fontSize: 13,
    textAlign: 'center',
    marginTop: 10,
    color: 'white'
  },
  imageIcon: {
    width: 90,
    height: 90,
    backgroundColor: 'white',
    borderRadius: 80,
    shadowOffset: { width: 10, height: 10, },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageIconChild: {
    width: 85,
    height: 85,
    backgroundColor: '#394a5e',
    borderRadius: 85,
    //   justifyContent: 'center',
    alignItems: 'center'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  category: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  paddingTopContent: {
    paddingTop: 5
  },
  categoryDot: {
    height: 5,
    width: 5,
    backgroundColor: '#bbb',
    borderRadius: 5
  },
  categoryText: {
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  categoryContent: {
    borderStyle: 'solid',
    borderLeftWidth: 0.7,
    borderLeftColor: '#bbb',
    marginLeft: 1.9
  },
  categoryContentItem: {
    flexDirection: 'column',
  },
  categoryTextContent: {

    paddingLeft: 20,
    padding: 10,
    justifyContent: 'center',
  },
  categoryChildText: {
    paddingLeft: 10
  },
  inforText: {
    fontSize: 12,
  }
};
