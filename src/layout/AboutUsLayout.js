
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import AboutUsComponent from '../components/AboutUsComponent';

export default class SupportLayout extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <Text style={{ paddingLeft: 60, color: '#fff', fontSize: 20, fontWeight: 'bold' }}>Về chúng tôi</Text>
        ),
        headerStyle: {
            backgroundColor: '#a01f1a',
        },
        headerTintColor: 'white',
    });
  render() {
    return (
      <View style={styles.container}>
        <AboutUsComponent {...this.props} />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
};
