import React, { Component } from 'react';
import { View, Text, PanResponder, LayoutAnimation, Image, StyleSheet, Animated, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import uri from 'rn-fetch-blob/utils/uri';
import Dimensions from 'Dimensions';
const DEVICE_WIDTH = Dimensions.get('window').width;
const DIVICE_HEIGHT = Dimensions.get('window').height;


const ANIMATION_DURATION = 20;
const ROW_HEIGHT = 70;

class CategoryMeetingList extends Component {

    constructor(props) {
        super(props);
        console.log(this.props);
        this.state = {
            y: null,
            marginY: new Animated.Value(0),
            animatedShow: new Animated.Value(0),
            transform: new Animated.Value(0),
            fadeAnim: new Animated.Value(0),
            height: 0,
            user: null
        }
    }

    componentWillMount() {

    }

    componentWillUpdate() {
        const config = {
            duration: 950,
            update: {
                type: 'spring',
                springDamping: 0.4,
            },
        };
        LayoutAnimation.configureNext(config);
    }

    componentDidMount() {
        this.initStart();
        const animate = Animated.timing(this.state.animatedShow, {
            toValue: 1,
            duration: ANIMATION_DURATION
        });
        Animated.loop(animate).start();
        Animated.timing(                  // Animate over time
            this.state.fadeAnim,            // The animated value to drive
            {
                toValue: 1,                   // Animate to opacity: 1 (opaque)
                duration: 10000,              // Make it take a while
            }
        ).start();
        const anim1 = Animated.timing(
            this.state.transform, {
                toValue: 1,
                duration: 1000
            }
        );
        const anim2 = Animated.timing(
            this.state.transform, {
                toValue: 0,
                duration: 1000
            }
        );
        const finalAnim = Animated.sequence([anim1, anim2]);
        Animated.loop(finalAnim).start();
    }

    initStart = async () => {
        try {
            const value = await AsyncStorage.getItem('@user');
            console.log(value);
            if (value != null) {
                const data = JSON.parse(value);
                this.setState({ user: data });
            }
        } catch (error) {
            // Error retrieving data
        }

    }

    _onMove = (event) => {
        const { locationY } = event.nativeEvent;
        const { onMove } = this.props;
        onMove();
        this.setState({ y: '322' });
        Animated.timing(this.state.animatedShow, {
            toValue: 1,
            duration: ANIMATION_DURATION
        }).start();

    }

    _onPressItemHead = (item) => {
        const { onMove } = this.props;
        // this.state.animation.setValue(this.state.height);
        // Animated.spring(
        //     this.state.animation,
        //     {
        //         toValue: this.state.height
        //     }
        // ).start();
        onMove();
    }

    _onPressItem = (item) => {
        const { navigate } = this.props.navigate;
        return navigate('MeetingLayout', { 
            item: item, 
            userid: this.state.user != null ? this.state.user.id : this.props.navigation.state.params.data.id,
            uuid : this.state.user != null ? this.state.user.uuid : this.props.navigation.state.params.data.uuid
        });
        //return navigate('MeetingLayout', { item: item, userid:  this.props.navigate.state.params.data.id });
        
    }

    _setMinHeight(event) {
        const { length } = this.props;
        const { FHeight } = this.props;

        if (event.nativeEvent.layout.height != 0) {

            const heightContent = DIVICE_HEIGHT - (event.nativeEvent.layout.height * 3) - 81;
            this.setState({
                height: heightContent
            });
        } else {
            const heightContent = DIVICE_HEIGHT - (60.5 * (length - 1)) - 81;
            this.setState({
                height: heightContent
            });
        }
    }


    render() {

        const { id, name, index, icon_path, description, color_hex } = this.props;
        const { status } = this.props;
        const { source } = this.props;
        const item = {
            id: id,
            name: name,
            index: index,
            icon_path: icon_path,
            description: description,
            color_hex: color_hex,
            source: source
        }



        const rowStyles = [
            styles.row,
            {
                //  opacity: this.state.animatedShow
            },
            {
                // transform: [
                //     { scale: this.state.animatedShow },
                //     {
                //         rotate: this.state.animatedShow.interpolate({
                //             inputRange: [0, 0.5, 1],
                //             outputRange: ['15deg', '0deg', '-15deg']
                //         })
                //     }
                // ],
            },
        ]

        const rotate = this.state.transform.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: ['15deg', '0deg', '-15deg']
        })

        let { fadeAnim } = this.state;
        return (

            <View style={styles.servicesStyle}>
                <View>
                    <View
                        onStartShouldSetResponder={() => false}
                        onMoveShouldSetResponder={() => true}
                        onResponderMove={this._onMove.bind(this)}
                        onLayout={this._setMinHeight.bind(this)}
                    >
                        {
                            item.source !== '' ? (
                                <ImageBackground
                                    style={{ flex: 1 }}
                                    source={item.source}
                                >
                                    {
                                        status[item.index] ? (
                                            <View></View>
                                        )
                                            : (
                                                <TouchableOpacity

                                                    onPress={() => this._onPressItemHead(item)}
                                                >
                                                    <View style={[styles.textContentStyle, { backgroundColor: item.color_hex, }]}>
                                                        <Text style={styles.textStyle}> {item.name.toUpperCase()}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                    }
                                </ImageBackground>
                            ) : (
                                    <ImageBackground
                                        style={{ flex: 1 }}
                                        source={{ uri: `${item.icon_path}` }}>
                                        {
                                            status[item.index] ? (
                                                <View></View>
                                            )
                                                : (
                                                    <TouchableOpacity

                                                        onPress={() => this._onPressItemHead(item)}
                                                    >
                                                        <View style={[styles.textContentStyle, { backgroundColor: item.color_hex, }]}>
                                                            <Text style={styles.textStyle}> {item.name.toUpperCase()}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                )
                                        }
                                    </ImageBackground>
                                )
                        }

                    </View>
                    {

                        status[item.index] ? (
                            <View>
                                <Animated.View style={{ flex: 1, }}>
                                    <TouchableOpacity onPress={() => this._onPressItem(item)}>
                                        <View
                                            style={[styles.thumbnaiContainerlStyle, { height: this.state.height }]}
                                        >
                                            {
                                                item.source !== null ? (
                                                    <ImageBackground
                                                        style={{ width: '100%', flex: 1 }}
                                                        source={item.source}
                                                    >
                                                        <View
                                                            style={styles.backgroundColorHead}
                                                        >
                                                            <ImageBackground
                                                                style={{ width: '100%', flex: 1 }}
                                                                source={require('../images/bg_gradient.png')}
                                                            >
                                                                <View style={styles.textImf}>
                                                                    <Text style={[styles.textStyle, { color: '#ec1c24', paddingBottom: 10, fontSize: 20 }]}> {item.name.toUpperCase()}</Text>
                                                                    <Image
                                                                        style={styles.thumbnailStyleNext}
                                                                        source={require('../images/Asset_2.png')} />
                                                                </View>
                                                            </ImageBackground>
                                                        </View>

                                                    </ImageBackground>
                                                ) : (
                                                        <ImageBackground
                                                            style={{ width: '100%', flex: 1 }}
                                                            source={{ uri: `${item.icon_path}` }}
                                                        //source={require(IMAGEBG[index])}>
                                                        >
                                                            <View
                                                                style={styles.backgroundColorHead}
                                                            >
                                                                <ImageBackground
                                                                    style={{ width: '100%', flex: 1 }}
                                                                    source={require('../images/bg_gradient.png')}>
                                                                    <View style={styles.textImf}>
                                                                        <Text style={[styles.textStyle, { color: '#ec1c24', paddingBottom: 10, fontSize: 20 }]}> {item.name.toUpperCase()}</Text>
                                                                        <Image
                                                                            style={styles.thumbnailStyleNext}
                                                                            source={require('../images/Asset_2.png')} />
                                                                    </View>
                                                                </ImageBackground>
                                                            </View>

                                                        </ImageBackground>
                                                    )
                                            }

                                        </View>
                                    </TouchableOpacity>
                                </Animated.View>
                            </View>
                        ) : (
                                <View>
                                </View>
                            )
                    }


                </View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    servicesStyle: {
        flex: 1,
        //margin: 5,
        // width: (DEVICE_WIDTH / 2) - 2,
        //  margin: 1
    },
    row: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        alignItems: 'center',
        // height: ROW_HEIGHT,
    },
    textContentStyle: {
        //backgroundColor: '#8bc53f',
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 15,
        opacity: 1
    },
    thumbnaiContainerlStyle: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        left: 0
    },
    thumbnailStyle: {
        flex: 1,
        width: 350,
        // height: 700,
        resizeMode: 'stretch'
    },
    thumbnailStyleNext: {
        //  textAlign: 'center',
        width: 25,
        height: 25,
        justifyContent: 'center',
        //    flex: 1,

        //   resizeMode: 'stretch'
    },
    backgroundColorHead: {
        position: 'absolute',
        justifyContent: 'center',
        //   opacity: 0.8,
        alignItems: 'center',
        bottom: 0,
        //    backgroundColor: '#333',
        width: '100%',
        height: '35%'
    },
    textImf: {
        position: 'absolute',
        bottom: 15,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
};

export default CategoryMeetingList;