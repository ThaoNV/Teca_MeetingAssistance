import React from 'react';
import { View, TouchableOpacity } from 'react-native';

const CardItem = ({onPress, children}) => {
    return (
        <View style={styles.containerStyle}>
            <TouchableOpacity onPress={onPress}>
                {children}
            </TouchableOpacity>
        </View>
    );
}

const styles = {
    containerStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        position: 'relative'
    }
}

export default CardItem;