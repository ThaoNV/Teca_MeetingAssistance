import React from 'react';
import { View } from 'react-native';

const CardItemView = ({children}) => {
    return (
        <View style={styles.containerStyle}>
                {children}
        </View>
    );
}

const styles = {
    containerStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        position: 'relative'
    }
}

export default CardItemView;