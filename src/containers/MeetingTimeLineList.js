import React, { Component } from 'react';
import { View, Text, PanResponder, LayoutAnimation, Image, StyleSheet, Animated, TextInput, TouchableOpacity, Platform, ScrollView, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';
import Dimensions from 'Dimensions';
import uri from 'rn-fetch-blob/utils/uri';
import RNCalendarEvents from 'react-native-calendar-events';
import HTML from 'react-native-render-html';
import Modal from 'react-native-modal';
import ImageSvg from 'react-native-remote-svg';
import { meetingCalendarTimeLine, meetingTimeLineNote } from '../actions/MeetingTimeLineAction';
//import { meetingTimeLineCalendar, meetingTimeLineUserNote } from '../api/meetingTimeLine';

const DEVICE_WIDTH = Dimensions.get('window').width;
const HEIGHT_DIVICE = Dimensions.get('window').height;

const ANIMATION_DURATION = 20;
const ROW_HEIGHT = 70;

class MeetingTimeLineList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleModal: null,
            text: '',
            isChange: false,
            height: 0,
            meetingTimeLineCalendar: meetingCalendarTimeLine,
            meetingTimeLineUserNote: meetingTimeLineNote
        }

    }
    componentDidMount() {
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidHide() {
        Keyboard.dismiss();
    }

    _onPressItem = (item) => {
        const { onMove } = this.props;
        onMove();
    }

    _showModel = (data, index) => {
        this.setState({ visibleModal: index, text: data.note, isChange: false })
        // console.log(this);

    }



    render() {

        //  this.state.meetingTimeLineCalendar('0','0', '0', '0');
        const { index, calendar_id, content, datetime, datetime_from_ts, datetime_to_ts, id, is_calendar_checked, note, time_from, time_to, title, type } = this.props;
        const { status } = this.props;
        // const { meetingCalendarTimeLine, meetingTimeLineCheckCalendar, meetingTimeLineUnCheckCalendar, meetingTimeLineNote } = this.props;
        const { MeetingTimeLineCheckCalendar, MeetingTimeLineUnCheckCalendar, SaveNote } = this.props;
        const item = {
            calendar_id: calendar_id,
            content: content,
            datetime: datetime,
            datetime_from_ts: datetime_from_ts,
            datetime_to_ts: datetime_to_ts,
            is_calendar_checked: is_calendar_checked,
            time_from: time_from,
            time_to: time_to,
            note: note,
            title: title,
            id: id,
            index,
            type: type
        }
        this._checkBoxCalendar = (data, index) => {
            console.log(data);
            let startDate = new Date(data.datetime_from_ts);
            let endDate = new Date(data.datetime_to_ts);
            RNCalendarEvents.authorizeEventStore();
            RNCalendarEvents.saveEvent(data.title, {
                startDate: startDate.toISOString(),
                endDate: endDate.toISOString(),
                location: ''
            }).then(event => {
                MeetingTimeLineCheckCalendar(event);
                this.state.meetingTimeLineCalendar(
                    `${event}`,
                    "check",
                    this.props.navigate.state.params.user_id,
                    data.id,
                    this.props.navigate.state.params.uuid
                );

            });
        }

        this._unCheckBoxCalendar = (data, index) => {
            MeetingTimeLineUnCheckCalendar();
            this.state.meetingTimeLineCalendar(
                data.calendar_id,
                "uncheck",
                this.props.navigate.state.params.user_id,
                data.id,
                this.props.navigate.state.params.uuid
            );
            RNCalendarEvents.authorizeEventStore();
            RNCalendarEvents.removeEvent(`${data.calendar_id}`).then(event => {
                console.log(event);
            })
                .catch(error => {
                    console.log(error);
                });
        }

        this._saveNote = (data, index) => {
            if (this.state.isChange) {
                SaveNote(this.state.text);
                this.state.meetingTimeLineUserNote(index, this.props.navigate.state.params.user_id, data.id, this.state.text);
                this.setState({ visibleModal: null });
                this._keyboardDidHide();
            }
        }

        this._onBackdropPress = (data, index) => {
            if (this.state.isChange) {
                Alert.alert(
                    'Thông báo',
                    'Bạn có muốn lưu nội dung ghi chú!',
                    [
                        { text: 'Đóng', onPress: () => { this.setState({ visibleModal: null }) } },
                        { text: 'Lưu', onPress: () => this._saveNote(data, index) },
                    ],
                    { cancelable: false }
                )
            } else {
                this.setState({ visibleModal: null });
            }
        }

        return (
            <View style={styles.servicesStyle}>
                <View>
                    <TouchableOpacity onPress={() => this._onPressItem(item)}>
                        <View style={styles.textContentStyle}>
                            <Text style={styles.textStyle}> {item.datetime}</Text>
                            <Text style={styles.textStyle}> {item.title}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {
                    status[item.index] ? (
                        <View style={styles.panel}>
                            {/* <View style={styles.panelHeading}>
                                <Text style={styles.textDate}>{item.datetime}</Text>
                                <Text style={styles.textTile}>{item.title}</Text>
                                <View style={styles.textLocation}>
                                    <Text style={styles.textLocationTxt}>Địa điểm: Phòng họp Diên Hồng</Text>
                                </View>
                            </View> */}
                            <View style={[styles.panelHeading, { flexDirection: 'row', paddingLeft: 5 }]}>
                                {
                                    item.is_calendar_checked == 0 ? (
                                        <TouchableOpacity style={styles.panelCalendar} onPress={() => this._checkBoxCalendar(item, index)}>
                                            <Image style={{ width: 25, height: 25 }} source={require('../images/lichcanhan.png')} />
                                            <Text style={styles.textLocationTxt}>Lịch cá nhân</Text>
                                        </TouchableOpacity>
                                    )
                                        : (
                                            <TouchableOpacity style={styles.panelCalendar} onPress={() => this._unCheckBoxCalendar(item, index)}>
                                                <Image style={{ width: 25, height: 25 }} source={require('../images/lich_red.png')} />
                                                <Text style={styles.textLocationTxt}>Lịch cá nhân</Text>
                                            </TouchableOpacity>
                                        )
                                }
                                <TouchableOpacity style={[styles.panelCalendar, { marginLeft: 10 }]} onPress={() => this._showModel(item, index)}>
                                    <Image style={{ width: 25, height: 25 }} source={require('../images/ghinho.png')} />
                                    <Text style={styles.textLocationTxt}>Ghi nhớ riêng</Text>
                                </TouchableOpacity>
                                <Modal
                                    isVisible={this.state.visibleModal === item.index}
                                    animationType="fade"
                                    closeOnClick={true}
                                    transparent={true}
                                    onBackdropPress={() => this._onBackdropPress(item, index)}
                                    onRequestClose={() => { this.setState({ visibleModal: null }) }}
                                    style={styles.bottomModal}>
                                    <View style={styles.noteContainer}>
                                        <ScrollView
                                            keyboardDismissMode="on-drag"
                                            keyboardShouldPersistTaps='always' >
                                            <View style={styles.noteContent}>
                                                <View style={styles.noteText}>
                                                    <TouchableWithoutFeedback accessible={false}>
                                                        <View style={{ flex: 1 }}>
                                                            <TextInput
                                                                style={[styles.defaultText, { height: Math.max(35, this.state.height) }]}
                                                                multiline={true}
                                                                onSubmit={Keyboard.dismiss}
                                                                onContentSizeChange={(event) => {
                                                                    this.setState({ height: event.nativeEvent.contentSize.height })
                                                                }}
                                                                onChangeText={(text) => {
                                                                    this.setState({ text: text, isChange: true })
                                                                }}
                                                                value={this.state.text}
                                                                placeholderTextColor="#616770"
                                                                placeholder="Nhập nội dung cần ghi nhớ"
                                                                underlineColorAndroid="transparent"
                                                            />
                                                        </View>
                                                    </TouchableWithoutFeedback>
                                                    <View
                                                        style={{
                                                            position: 'absolute',
                                                            top: 15,
                                                            right: 18,
                                                        }}
                                                    >
                                                        {
                                                            (this.state.text !== '' && this.state.isChange) ? (
                                                                <TouchableOpacity onPress={() => this._saveNote(item, index)}>
                                                                    <Image style={{ width: 25, height: 25 }} source={require('../images/save2.png')} />
                                                                </TouchableOpacity>
                                                            ) : (
                                                                    <Image style={{ width: 25, height: 25 }} source={require('../images/save1.png')} />
                                                                )
                                                        }

                                                        {/* <ImageSvg
                                                        style={{ width: 25, height: 25 }}
                                                        source={this.state.text !== '' ? require('../images/save2.svg') : require('../images/save.svg')}
                                                    /> */}


                                                    </View>
                                                </View>
                                                {/* <View style={styles.noteSave}>
                                                <TouchableOpacity onPress={() => this._saveNote(item, index)}>
                                                    <Text>Lưu</Text>
                                                </TouchableOpacity>
                                            </View> */}
                                            </View>
                                            {/* <TouchableOpacity onPress={() => {
                                                this.setState({ visibleModal: null })
                                            }}>
                                                <View style={styles.noteHead}>
                                                    <TouchableOpacity onPress={() => {
                                                        this.setState({ visibleModal: null })
                                                    }}>
                                                        <Image style={{ width: 10, height: 10 }} source={require('../images/if_close_326579.png')} />
                                                    </TouchableOpacity>
                                                </View>
                                            </TouchableOpacity> */}
                                        </ScrollView>

                                    </View>
                                </Modal>
                            </View>
                            <View style={styles.categoryContent}>

                                <View style={styles.categoryContentItem}>
                                    <View style={styles.categoryTextContent}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.categoryChildText}>Thời gian: {item.datetime}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.categoryTextContent}>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={styles.categoryChildText} >Nội dung: </Text>
                                            {
                                                item.content !== null ? (
                                                    <HTML style={styles.categoryChildText} html={`<div>${item.content}</div>`} {...this.props} />

                                                ) : (
                                                        <View></View>
                                                    )
                                            }


                                        </View>
                                    </View>
                                </View>
                            </View>

                        </View>
                    ) : (
                            <View></View>
                        )
                }
            </View>
        )
    }
}

const styles = {
    servicesStyle: {
        flex: 1,
        //  flexDirection: 'row',
        width: '100%'
        // flexWrap: 'wrap'
    },
    textContentStyle: {
        backgroundColor: '#091d2d',
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#d0d2d3',
        borderBottomWidth: 1
    },
    textStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 13
    },
    // panel: {
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // },
    panelCalendar: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    panelHeading: {
        flex: 1,
        backgroundColor: 'rgb(255, 255, 255)',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        //   textAlign: 'center',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    textDate: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 12,
        color: '#ec1c24'
    },
    defaultText: {
        backgroundColor: '#f2f3f5',
        borderRadius: 18,
        width: DEVICE_WIDTH - 20,
        height: 40,
        borderColor: '#ccd0d5',
        borderWidth: 1,
        paddingLeft: 15,
        paddingRight: 45,
    },
    textTile: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 12,
        color: '#404041',
        fontWeight: 'bold',

    },
    textLocation: {
        flex: 1,
        flexDirection: 'column',
    },
    imageLocation: {
        flex: 1,
        width: 20,
        height: 20,
    },
    textLocationTxt: {
        fontSize: 14,
    },
    containerHead: {
        backgroundColor: '#333',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        padding: 10,
        height: 40
    },
    textHead: {
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white'
    },
    containerContent: {
        padding: 10,
    },
    subTitle: {
        flexDirection: 'column'
    },
    subItem: {
        flexDirection: 'row'
    },
    subDetailLeft: {
        width: 150
    },
    subDetailRight: {
        //paddingLeft: 30
    },
    subTextLeft: {
        color: '#333',
        fontWeight: 'bold'
    },
    button: {
        backgroundColor: 'white',
        padding: 20
    },
    text: {
        fontSize: 13,
        textAlign: 'center',
        marginTop: 10,
        color: 'white'
    },
    imageIcon: {
        width: 90,
        height: 90,
        backgroundColor: 'white',
        borderRadius: 80,
        shadowOffset: { width: 10, height: 10, },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageIconChild: {
        width: 85,
        height: 85,
        backgroundColor: '#394a5e',
        borderRadius: 85,
        //   justifyContent: 'center',
        alignItems: 'center'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    category: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    categoryDot: {
        height: 5,
        width: 5,
        backgroundColor: '#bbb',
        borderRadius: 5
    },
    categoryText: {
        paddingLeft: 10,
        fontWeight: 'bold',
    },
    categoryContent: {
        padding: 10,
        backgroundColor: '#f1f1f1'
        // borderStyle: 'solid',
        // borderLeftWidth: 0.7,
        // borderLeftColor: '#bbb',
        // marginLeft: 1.9
    },
    categoryContentItem: {
        flexDirection: 'column',
    },
    categoryTextContent: {
        justifyContent: 'center',
    },
    categoryChildText: {
        fontSize: 14,
    },
    inforText: {
        fontSize: 12,
    },
    bottomModal: {
        // justifyContent: 'flex-end',
        margin: 0,
    },
    noteContainer: {
        position: 'absolute',
        top: 0,
        backgroundColor: '#fff',
        flexDirection: 'column',
        borderBottomColor: '#d0d2d3',
        borderBottomWidth: 1,
        ...Platform.select({
            ios: {
                top: 10
            },
            android: {
                top: 0
            }
        })
    },
    noteHead: {
        padding: 5,
        backgroundColor: '#f1f1f1',
        borderBottomColor: '#d0d2d3',
        borderBottomWidth: 1,
        alignItems: 'flex-end',
        // justifyContent: 'center',

    },
    noteContent: {
        flexDirection: 'row'
    },
    noteText: {
        padding: 10
    },
    noteSave: {
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    }
};

export default MeetingTimeLineList;